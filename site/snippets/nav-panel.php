<nav class="nav-panel" id="navigation-panel">
  <button class="nav-panel__close">
    <img src="<?= url('assets') ?>/svg/close.svg" alt="Icône de fermeture du panneau de navigation">
  </button>
  <ul>
  <?php foreach($site->children()->without('home')->without('error') as $page): ?>
    <li style="--color: <?= getColor($page) ?>">
      <a class="nav-panel__page" href="<?= $page->template() == 'category' ? $page->children()->first()->url() : $page->url() ?>"><?= $page->title() ?></a>
      <?php if ($page->hasChildren()): ?>
          <ul>
            <?php foreach($page->children() as $subpage): ?>
              <li>
                <a class="nav-panel__subpage" href="<?= $subpage->url() ?>"><?= $subpage->title() ?></a>
                <?php if ($subpage->hasSections()): ?>
                    <ul>
                      <?php foreach($subpage->sections() as $section): ?>
                          <li>
                            <a class="nav-panel__section" href="<?= $subpage->url() . '/#' . $section->id() ?>"><?= $section->title() ?></a>
                          </li>
                      <?php endforeach ?>
                    </ul>
                <?php endif ?>
              </li>
            <?php endforeach ?>
          </ul>
      <?php endif ?>
    </li>
  <?php endforeach ?>
  </ul>
  <ul class="socials">
    <?php if ($site->twitter()->isNotEmpty()): ?>
      <li>
          <a href="<?= $site->twitter() ?>" title="Aller au compte Twitter / X" target="_blank">
            <img src="<?= url('assets') ?>/images/icons/x.svg" alt="Logo de Twitter / X">
          </a>
      </li>
    <?php endif ?>
    <?php if ($site->facebook()->isNotEmpty()): ?>
      <li>
          <a href="<?= $site->facebook() ?>" title="Aller au compte Facebook" target="_blank">
            <img src="<?= url('assets') ?>/images/icons/facebook.svg" alt="Logo de Facebook">
          </a>
      </li>
    <?php endif ?>
    <?php if ($site->instagram()->isNotEmpty()): ?>
      <li>
          <a href="<?= $site->instagram() ?>" title="Aller au compte Instagram" target="_blank">
            <img src="<?= url('assets') ?>/images/icons/instagram.svg" alt="Logo d'Instagram">
          </a>
      </li>
    <?php endif ?>
    <?php if ($site->mastodon()->isNotEmpty()): ?>
      <li>
          <a href="<?= $site->mastodon() ?>" title="Aller au compte Mastodon" target="_blank">
            <img src="<?= url('assets') ?>/images/icons/mastodon.svg" alt="Logo de Mastodon">
          </a>
      </li>
    <?php endif ?>
  </ul>
</nav>