<?php
  $columnsCount = count($layout->columns());
  $representativesCrop = null;
  if ($columnsCount > 1) {
    $representativesCrop = [
      "width" => $columnsCount === 2 ? 1100 : 700,
      "height" => $columnsCount === 2 ? 580 : 700,
    ];
  }
?>
<section 
  class="row 
  <?= e(
      $layout->attrs()->isNotDivided() == 'true' 
      || $page->template() == 'home' 
      || $page->template() == 'repository'
      , 
      'row--not-divided'
    ) ?>">
  <?= e($layout->attrs()->title()->isNotEmpty(), '<h2>' . $layout->attrs()->title() . '</h2>') ?>
  <div class="
    grid 
    grid--<?= $columnsCount ?>
  " <?= e($layout->attrs()->isInNav() == 'true', 'id="' . Str::slug($layout->attrs()->sectionTitle()) . '"') ?>>
    <?php foreach ($layout->columns() as $column): 
      $isClamped = $columnsCount == 2 && $column->span() == '4';
    ?>
    <div 
      class="column 
        <?= $column->span() == 6 ? 'no-line column--large' : '' ?> 
        <?= $column->span() == 3 ? 'no-line column--small' : '' ?>
        <?= $column->span() <= 4 ? 'no-line column--truncable' : '' ?>
      " 
      style="--span:<?= $column->span() ?>"
    >
      <div class="blocks">
        <?php foreach($column->blocks() as $block): ?>
          <?php if ($block->type() === "representative"): ?>
            <?php snippet('blocks/representative', ["block" => $block, "crop" => $representativesCrop]) ?>
          <?php else: ?>
            <?= $block ?>
          <?php endif ?>
        <?php endforeach ?>
      </div>
    </div>
    <?php endforeach ?>
  </div>
</section>