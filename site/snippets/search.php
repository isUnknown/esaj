<div x-data="{ 
  open: false,
  openSearch() {
    this.open = true
    $refs.input.focus()
  },
  closeSearch() {
    this.search = ''
    this.open = false
  }
}" class="search" :class="open ? 'open' : 'close'" @click.outside="open = false" @click="openSearch()">
  <img class="search__icon" src="/assets/svg/search.svg" aria-hidden="true">
  <input id="search-field" class="search__field" type="text" placeholder="<?= $placeholder ?>" x-model="search" @keyup.escape="closeSearch()" x-ref="input">
  <button 
    class="search__clear" 
    aria-label="Nettoyer le champ de recherche" 
    aria-controls="search-field" 
    :class="search.length === 0 ? 'hidden' : ''" 
    @click="search = ''"
  >
    <img src="/assets/svg/close.svg" alt="Icône de nettoyage du champ">
  </button>
</div>