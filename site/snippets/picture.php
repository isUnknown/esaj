<?php if (isset($file)): ?>
    <?php
        $sizes = isset($size) ? '(min-width: 1085px) ' . $size . 'vw, 100vw' : '(min-width: 1085px) 50vw, 100vw';
        $alt = $alt ?? $file->alt();
        $file = isset($crop) ? $file->crop($crop['width'], $crop['height']) : $file;

        $webPSrcset = $file->srcset('webp');
        $srcset = $file->srcset();
        $src = $file->url();
        $width = $file->resize(1800)->width();
        $height = $file->resize(1800)->height();
        $class = isset($class) ? 'class="' . $class . '"': '';
        $lazy = $lazy ?? true;
    ?>



    <picture <?= $class ?>>
        
        <source srcset="<?= $webPSrcset ?>"
            sizes="<?= $sizes ?>" type="image/webp">
        <img 
            src="<?= $src ?>"
            srcset="<?= $srcset ?>"
            sizes="<?= $sizes ?>"
            width="<?= $width ?>"
            height="<?= $height ?>"
            alt="<?= $alt?>"
        >
        <div class="loader"></div>
    </picture>
<?php endif ?>
