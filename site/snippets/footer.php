<footer class="main-footer">
  <?php snippet('cookie-banner') ?>
  <div class="identity">
    <figure>
      <img
        src="<?= url('assets') ?>/images/logo.svg"
        alt="Logo de l'ESAJ">
      <figcaption>Établissement<br>
        d'enseignement<br>
        supérieur privé</figcaption>
    </figure>
  </div>
  <ul class="contact">
    <li><?= $site->street() ?></li>
    <li><?= $site->postCode() ?>
      <?= $site->city() ?>
    </li>
    <li><a href="tel:<?= $site->tel() ?>"
        title="Appeler le <?= $site->tel() ?>"><?= $site->tel() ?></a>
    </li>
    <li><a href="mailto:<?= $site->mail() ?>"
        title="Envoyer un mail à <?= $site->mail() ?>"><?= $site->mail() ?></a>
    </li>
  </ul>
  <ul class="links">
    <?php foreach($site->footerLinks()->toStructure() as $item): ?>
    <li>
      <a href="<?= $item->link()->toUrl() ?>"
        title="Aller à <?= $item->link()->toUrl() ?>"
        <?= e(!str_contains($item->link()->toUrl(), $site->url()), ' target="_blank"') ?>
        >
        <?= $item->label() ?>
      </a>
    </li>
    <?php endforeach ?>
  </ul>
  <ul class="socials">
    <?php if ($site->facebook()->isNotEmpty()): ?>
    <li>
      <a href="<?= $site->facebook() ?>"
        title="Aller au compte Facebook" target="_blank">
        <img
          src="<?= url('assets') ?>/images/icons/facebook.svg"
          alt="Logo de Facebook">
      </a>
    </li>
    <?php endif ?>
    <?php if ($site->instagram()->isNotEmpty()): ?>
    <li>
      <a href="<?= $site->instagram() ?>"
        title="Aller au compte Instagram" target="_blank">
        <img
          src="<?= url('assets') ?>/images/icons/instagram.svg"
          alt="Logo d'Instagram">
      </a>
    </li>
    <?php endif ?>
    <?php if ($site->linkedIn()->isNotEmpty()): ?>
    <li>
      <a href="<?= $site->linkedIn() ?>"
        title="Aller au compte linkedIn" target="_blank">
        <img
          src="<?= url('assets') ?>/images/icons/linkedin.svg"
          alt="Logo de Linkedin">
      </a>
    </li>
    <?php endif ?>
    <?php if ($site->tiktok()->isNotEmpty()): ?>
    <li>
      <a href="<?= $site->tiktok() ?>" title="Aller au compte TikTok"
        target="_blank">
        <img
          src="<?= url('assets') ?>/images/icons/tiktok.svg"
          alt="Logo de TikTok">
      </a>
    </li>
    <?php endif ?>
    <?php if ($site->youtube()->isNotEmpty()): ?>
    <li>
      <a href="<?= $site->youtube() ?>"
        title="Aller au compte Youtube" target="_blank">
        <img
          src="<?= url('assets') ?>/images/icons/youtube.svg"
          alt="Logo de Youtube">
      </a>
    </li>
    <?php endif ?>
  </ul>
  <?php
    snippet('edit-button', [
        'text' => 'Éditer la page',
    ])
  ?>
</footer>
<?php snippet('seo/schemas'); ?>
<script>
  function loadScript(a) {
    var b = document.getElementsByTagName("head")[0],
      c = document.createElement("script");
    c.type = "text/javascript", c.src = "https://tracker.metricool.com/resources/be.js", c.onreadystatechange = a, c
      .onload = a, b.appendChild(c)
  }
  loadScript(function() {
    beTracker.t({
      hash: "fbff3048c15bdc43f9088d44d9d1d616"
    })
  });
</script>
</body>

</html>