<?php if ($page->buttonLinks()->isNotempty()): ?>
  <section class="links">
    <ul>
    <?php foreach($page->buttonLinks()->toStructure() as $item): ?>
      <?php if ($item->link()->isNotEmpty()): ?>
        <li>
          <a 
            class="nav-btn" 
            href="<?= $item->link()->toUrl() ?>" 
            style="--color: <?= Str::lower($item->color()) ?>"
            title="Aller à <?= $item->link()->toUrl() ?>"
            target="<?= e(!str_contains($item->link()->toUrl(), $site->url()), '_blank') ?>"
          >
              <?= $item->label() ?>
          </a>
        </li>
      <?php endif ?>
    <?php endforeach ?>
    </ul>
    </section>
<?php endif ?>