<?php snippet('head') ?>
<body data-template="<?= $page->template() ?>">
  <header class="main-header <?= e($page->isHomePage(), 'main-header--home') ?>">
    <?php if ($page->isHomePage()): ?>
      <div class="identity">
        <a href="/" >
          <img src="<?= url('assets') ?>/images/logo-sos.svg" alt="Logo d'ESAJ">
          <h1><?= $site->subtitle() ?></h1>
        </a>
      </div>
    <?php endif ?>
    <nav class="main-nav">
      <a class="logo-small" href="/" title="Aller à l'accueil">
        <img src="<?= url('assets') ?>/images/logo.svg" alt="Logo d'ESAJ">
      </a>
      <ul class="main-column-width">
        <?php foreach($site->children()->without('home')->without('error') as $child): ?>
          <li>
            <a 
              class="nav-btn" 
              href="<?= $child->template() == 'category' ?
                $child->children()->first()->url()
                : $child->url() ?>" 
              style="--color: <?= Str::lower($child->color()) ?>"><?= $child->title() ?>
            </a>
            <?php if ($child->hasChildren()): ?>
              <div class="subpages">
                <?php foreach($child->children() as $subpage): ?>
                  <a 
                    class="nav-btn" 
                    href="<?= $subpage->url() ?>" 
                    style="--color: <?= Str::lower($child->color()) ?>"><?= $subpage->title() ?>
                  </a>
                <?php endforeach ?>
              </div>
            <?php endif ?>
          </li>
        <?php endforeach ?>
      </ul>
      
      <button 
        class="burger" 
        aria-label="Ouvrir le menu de navigation" 
        aria-controls="navigation-panel" 
        aria-expanded="false"
      >
        <span class="burger__bar"></span>
      </button>
      
      <div class="submenu-bg"></div>
    </nav>
    <?php snippet('nav-panel') ?>
  </header>