<?php
$tags = $slots->external() ? preg_replace('/\s+/', ' ', $slots->external()) : false;
$tags = $tags ? str_replace('</script>', '<\/script>', $tags) : false;
$internal = $internal ?? [];
?>
<script>
  if (sessionStorage.getItem("cookieAccepted")) {
    if (<?= isset($tags) === true ?> != false) {
        const div = document.createElement('div');
        div.innerHTML = '<?= $tags ?>';
        const externalScripts = div.querySelectorAll('*')
        const head = document.querySelector('head')
        
        externalScripts.forEach(externalScript => {
          head.appendChild(externalScript)
        })
      }

      <?php foreach($internal as $script): ?>
        <?= $script ?>
      <?php endforeach ?>
    }
</script>