<section class="cards">
  <ul class="grid">
    <?php foreach($page->featured()->toStructure() as $card): ?>
      <?php
        $linkedPage = $card->isExternal() == 'true' ? null : $card->linkedPage()->toPage();
        $isExternal = $card->isExternal() == 'true';
        

        $link = $isExternal ? $card->link() : $linkedPage->url();
        $image = array(
          "src" => $isExternal ? $card->cover()->toFile()->url() : $linkedPage->cover(),
          "alt" => $isExternal ? $card->cover()->toFile()->alt() : $linkedPage->cover()->alt()
        );
        $target = $isExternal ? '_blank' : "";
        $title = $isExternal ? $card->title() : $linkedPage->title();
        $text = $isExternal ? $card->text() : $linkedPage->intro();
        $colors = $site->colors()->toStructure()->toArray();
        $color = $isExternal ? getRandomColor() : $linkedPage->parent()->color();
        ?>
      
      <li class="card" >
        <?php snippet('card', array(
            'color' => $color,
            'target' => $target,
            'link' => $link,
            'image'  => $image,
            'title' => $title,
            'text' => $text,
          )) ?>
      </li>
    <?php endforeach ?>
  </ul>
</section>