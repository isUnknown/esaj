<?= e($block->isSticky() == 'true', '<div class="sticky">') ?>
<?= e($block->text()->length() > 400, '<div class="block--truncated">') ?>

  <?= $block->text(); ?>

  <?= e($block->text()->length() > 400, '</div>') ?>
<?= e($block->isSticky() == 'true', '</div>') ?>

<?php if ($block->text()->length() > 400): ?>
  <div class="read-more-wrapper">
    <button class="read-more">Lire la suite</button>
  </div>
<?php endif ?>

