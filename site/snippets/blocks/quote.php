<?= e($block->isSticky() == 'true', '<div class="sticky">') ?>
<?= e($block->text()->length() > 400, '<div class="block--truncated">') ?>

  <blockquote>
    <?= $block->text() ?>
    <?php if ($block->citation()->isNotEmpty()): ?>
    <footer>
      <?= $block->citation() ?>
    </footer>
    <?php endif ?>
  </blockquote>

<?= e($block->text()->length() > 400, '</div>') ?>
<?php if ($block->text()->length() > 400): ?>
  <div class="read-more-wrapper">
    <button class="read-more">Lire la suite</button>
  </div>
<?php endif ?>