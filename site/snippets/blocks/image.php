<?php

/** @var \Kirby\Cms\Block $block */
$alt     = $block->alt();
$caption = $block->caption();
$crop    = $block->crop()->isTrue();
$link    = $block->link();
$ratio   = $block->ratio()->or('auto');
$src     = null;

if ($block->location() == 'web') {
    $src = $block->src()->esc();
} elseif ($image = $block->image()->toFile()) {
    $alt = $alt->or($image->alt());
    $src = $image->thumb()->url();
}

?>
<?php if ($src): ?>
<figure<?= Html::attr(['data-ratio' => $ratio, 'data-crop' => $crop], null, ' ') ?>>
  <?php if ($link->isNotEmpty()): ?>
  <a href="<?= Str::esc($link->toUrl()) ?>">
    <img 
      src="<?= $src ?>" 
      srcset="<?= $image->srcset() ?>" 
      sizes="(max-width: 700px) 90vw, 40vw" 
      alt="<?= $alt->esc() ?>"
      width="<?= $image->thumb()->width() ?>"
      height="<?= $image->thumb()->height() ?>"
    >
  </a>
  <?php else: ?>
  <img 
    src="<?= $src ?>" 
    srcset="<?= $image->srcset() ?>" 
    sizes="(max-width: 700px) 90vw, 40vw" 
    alt="<?= $alt->esc() ?>"
    width="<?= $image->thumb()->width() ?>"
    height="<?= $image->thumb()->height() ?>"
  >
  <?php endif ?>

  <?php if ($caption->isNotEmpty()): ?>
  <figcaption>
    <?= $caption ?>
  </figcaption>
  <?php endif ?>
</figure>
<?php endif ?>