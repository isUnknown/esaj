<?php
$level = $block->level() == 'h1' ? 'h2' : $block->level()->or('h2');
?>
<<?= $level ?> class="font-<?= $block->font() ?><?= e($block->level() == 'h1', ' h1') ?>""><?= $block->text() ?></<?= $level ?>>