<?php 
$members = $block->rows()->toStructure();
if($members->isNotEmpty()):
?>

<ul>
  <?php foreach($members as $member): ?>
    <li 
      class="member" 
      <?php if ($member->presentation()->isNotEmpty()): ?>
        data-presentation="<?= $member->presentation()->escape() ?>"
      <?php endif ?>
      <?php if ($member->cover()->isNotEmpty()): ?>        
        data-cover-src="<?= $member->cover()->toFile()->thumb()->url() ?>" 
        data-cover-srcset="<?= $member->cover()->toFile()->srcset() ?>" 
        data-cover-sizes="(max-width: 700px) 90vw, 40vw" 
        data-cover-width="<?= $member->cover()->toFile()->thumb()->width() ?>"
        data-cover-height="<?= $member->cover()->toFile()->thumb()->height() ?>"
        data-cover-alt="<?= $member->cover()->toFile()->alt() ?>"
      <?php endif ?>
      <?php if ($member->link()->isNotEmpty()): ?>
        data-link="<?= $member->link()->toUrl() ?>"
      <?php endif ?>
    >
      <button class="member__button">
        <p class="member__role"><?= $member->role() ?></p>
        <span class="member__divider"></span>
        <p class="member__name"><?= $member->name() ?></p>
      </button> 
    </li>
  <?php endforeach ?>
</ul>

<?php endif; ?>