<?php
$message = $message ?? 'Ce site utilise des cookies pour améliorer votre expérience de navigation et réaliser des statistiques de visites.';
?>

<div class="simple-cookie-banner hidden">
  <p class="simple-cookie-banner__message"><?= $message ?></p>
  <div class="simple-cookie-banner__buttons">
    <button class="simple-cookie-banner__accept">accepter</button>
    <button class="simple-cookie-banner__deny">refuser</button>
  </div>
</div>