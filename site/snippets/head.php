<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>
    <?php if ($page->isHomePage() == false): ?>
    <?= $page->title() ?>&nbsp;|&nbsp;
    <?php endif ?>
    <?= $site->title() ?>
  </title>

  <link rel="stylesheet"
    href="<?= url('assets') ?>/css/style.css?version-cache-prevent<?= rand(0, 1000)?>">
  <link rel="stylesheet" type="text/css"
    href="<?= url('media/plugins/adrienpayet/edit-button/style.css') ?>">
  <script src="<?= url('assets') ?>/js/script.js"
    type="module" defer></script>

  <?= e($page->template() == 'directory' || $page->template() == 'jobs', '<script defer src="https://cdn.jsdelivr.net/npm/alpinejs@3.x.x/dist/cdn.min.js"></script>') ?>

  <meta name="robots" content="noindex">
  <?php snippet('favicon') ?>

  <?php snippet('seo/head'); ?>

  <?php if ($kirby->request()->is('POST') && get('submit')): ?>
      <?php snippet('cookie-scripts', [
        "internal" => [
          "gtag('event', 'conversion', {'send_to': 'AW-674861644/mC_kCNO6_p8ZEMyk5sEC'});"
        ]
      ]) ?>
  <?php endif ?>

  <?php snippet('cookie-scripts', [
      'internal' => [
          "window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
    
      gtag('config', 'AW-674861644');",
      
          "!function(f,b,e,v,n,t,s)
      {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};
      if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
      n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t,s)}(window, document,'script',
      'https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '1404258496670771');
      fbq('track', 'PageView');"
      ]
  ], slots: true) ?>
    <?php slot('external') ?>
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-674861644"></script>
    <noscript>
      <img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=1404258496670771&ev=PageView&noscript=1" />
    </noscript>
    <?php endslot() ?>

  <?php endsnippet() ?>



  <style>
    :root {
      <?php foreach ($site->colors()->toStructure() as $item) : ?>
      --color-
      <?= Str::lower($item->label()) ?>
      :
        <?= $item->color() ?>
      ;
      <?php endforeach ?>
    }
  </style>

  <?php snippet('front-comments') ?>
</head>