<?php
$target = $target ?? '';
?>

<div class="card" style="--color: <?= $color ?>">
  <figure class="image-wrapper">
    <?php if (!str_contains($color, "#fff")): ?>
      <div class="color-filter"></div>
    <?php endif ?>
    <?php if ($image != null): ?>

        <?php snippet('picture', ["file" => $image, 'size' => 40, 'alt' => $image->alt(), "crop" => isset($crop) ? $crop : null]); ?>
    <?php endif ?>
  </figure>
  <a href="<?= $link ?>" target="<?= $target ?>"><h3 class="card-title"><?= $title ?></h3></a>
  <p class="card-text">
    <?php if ($text->length() > 150): ?>
      <?= Str::unhtml($text->inline()->short(150)) ?>
      <?php if (!str_contains($link, $site->url())): ?>
        <button class="card__toggle-full-text">Lire la suite ↑</button>
        <div class="card-text__full">
          <a href="<?= $link ?>" target="<?= $target ?>"><h3 class="card-title"><?= $title ?></h3></a>
          <?= $text->kt() ?>
          <footer>
            <button class="card__toggle-full-text card__toggle-full-text--less">Fermer ↓</button>
          </footer>
        </div>
      <?php else: ?>
        <a class="card__toggle-full-text" href="<?= $link ?>">Lire la suite →</a>
      <?php endif ?>
    <?php else: ?>
      <?= $text->inline() ?>
    <?php endif ?>
  </p>
</div>