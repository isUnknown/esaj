<?php

return function ($kirby, $site, $pages, $page) {
    $alert = null;
    if($kirby->request()->is('POST') && get('submit')) {
        // check the honeypot
        if(empty(get('website')) === false) {
            go($page->url());
            exit;
        }

        $data = [
            'formation'    => $page->title()->value(),
            'phone'        => get('phone'),
            'address'      => get('address'),
            'postcode'     => get('postcode'),
            'city'         => get('city'),
            'state'        => get('state'),
            'first-name'   => get('first-name'),
            'last-name'    => get('last-name'),
            'email'        => get('email'),
            'actual-level' => get('actual-level'),
            'wanted-level' => get('wanted-level'),
        ];

        $rules = [
            'formation'    => ['required'],
            'first-name'   => ['required', 'minLength' => 3],
            'last-name'    => ['required', 'minLength' => 3],
            'email'        => ['required', 'email'],
            'actual-level' => ['required'],
            'wanted-level' => ['required'],
        ];

        $messages = [
            'formation'    => 'Veuillez sélectionner une formation.',
            'first-name'   => 'Prénom trop court.',
            'last-name'    => 'Nom de famille trop court.',
            'email'        => 'Adresse email invalide.',
            'actual-level' => 'Veuillez préciser votre niveau d\'étude actuel.',
            'wanted-level' => 'Veuillez préciser le niveau d\'entrée souhaité.',
        ];

        // some of the data is invalid
        if($invalid = invalid($data, $rules, $messages)) {
            $alert = $invalid;
        // the data is fine, let's send the email
        } else {
            try {
                $normalized_firstname = Str::slug($data['first-name']);
                $normalized_lastname = Str::slug($data['last-name']);
                $date = date('Y_m_d');
                $timestamp = date('His');
                $filename = $date . '-' . $timestamp . '-' . $normalized_firstname . '_' . $normalized_lastname . '.md';

                $emailContent = Str::upper('CANDIDATURE DE ' . esc($data['first-name']) . ' ' . esc($data['last-name']) . ' - ' . date('d/m/Y') . "\n") .

                    "\n-----------------------------------\n" .
                    Str::upper("INFORMATIONS PERSONNELLES :\n") .
                    'Formation souhaitée : ' . esc($data['formation']) . "\n" .
                    'Prénom : ' . esc($data['first-name']) . "\n" .
                    'Nom : ' . esc($data['last-name']) . "\n" .
                    'Email : ' . esc($data['email']) . "\n" .
                    'Téléphone : ' . (!empty($data['phone']) ? esc($data['phone']) : 'Non renseigné') . "\n" .
                    'Adresse : ' .
                    (!empty($data['address']) || !empty($data['postcode']) || !empty($data['city']) || !empty($data['state']) ?
                        (!empty($data['address']) ? esc($data['address']) . ', ' : '') .
                        (!empty($data['postcode']) ? esc($data['postcode']) . ' ' : '') .
                        (!empty($data['city']) ? esc($data['city']) . ' ' : '') .
                        (!empty($data['state']) ? '(' . esc($data['state']) . ')' : '') : 'Non renseignée') . "\n" .

                    "\n-----------------------------------\n" .
                    Str::upper("NIVEAUX ACADÉMIQUES :\n") .
                    'Niveau actuel : ' . esc($data['actual-level']) . "\n" .
                    'Niveau souhaité : ' . esc($data['wanted-level']) . "\n";

                file_put_contents($page->root() . '/' . $filename, $emailContent);

                $kirby->email([
                    'template' => 'submission--initial',
                    'from'     => 'candidature@esaj.asso.fr',
                    'replyTo'  => $data['email'],
                    'to'       => 'admin@esaj.asso.fr',
                    'subject'  => 'Nouvelle candidature : ' . esc($data['first-name']) . ' ' . esc($data['last-name']),
                    'data'     => [
                        'firstName'   => esc($data['first-name']),
                        'lastName'    => esc($data['last-name']),
                        'email'       => esc($data['email']),
                        'formation'   => esc($data['formation']),
                        'phone'       => esc($data['phone']) ?? false,
                        'address'     => esc($data['address']) ?? false,
                        'postcode'    => esc($data['postcode']) ?? false,
                        'city'        => esc($data['city']) ?? false,
                        'state'       => esc($data['state']) ?? false,
                        'actualLevel' => esc($data['actual-level']),
                        'wantedLevel' => esc($data['wanted-level']),
                    ],
                ]);
            } catch (Exception $error) {
                if(option('debug')):
                    $alert['error'] = 'Le formulaire n\'a pas pu être soumis: <strong>' . $error->getMessage() . '</strong>';
                else:
                    $alert['error'] = 'Le formulaire n\'a pas pu être soumis !';
                endif;
            }

            if (empty($alert) === true) {
                $success = 'Votre candidature a bien été envoyée. Nous vous contacterons dans les meilleurs délais.';
                $data = [];
            }
        }
    }

    return [
        'alert'   => $alert,
        'data'    => $data ?? false,
        'success' => $success ?? false,
    ];
};
