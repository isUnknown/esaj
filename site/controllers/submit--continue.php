<?php

return function ($kirby, $site, $pages, $page) {

    $alert = null;

    if($kirby->request()->is('POST') && get('submit')) {

        // check the honeypot
        if(empty(get('website')) === false) {
            go($page->url());
            exit;
        }

        $data = [
            'formation'       => get('formation'),
            'phone'           => get('phone'),
            'address'         => get('address'),
            'postcode'        => get('postcode'),
            'city'            => get('city'),
            'state'           => get('state'),
            'first-name'      => get('first-name'),
            'last-name'       => get('last-name'),
            'email'           => get('email'),
            'from'            => get('from'),
            'jobs'            => get('jobs'),
            'company-name'    => get('company-name'),
            'company-type'    => get('company-type'),
            'company-address' => get('company-address'),
            'company-phone'   => get('company-phone'),
            'company-siret'   => get('company-siret'),
            'financier'       => get('financier'),
            'comment'         => get('comment'),
        ];

        $rules = [
            'formation'    => ['required'],
            'first-name'   => ['required', 'minLength' => 3],
            'last-name'    => ['required', 'minLength' => 3],
            'email'        => ['required', 'email'],
            'jobs'         => ['required'],
            'company-name' => ['required'],
            'company-type' => ['required'],
            'financier'    => ['required'],
        ];

        $messages = [
            'formation'    => 'Veuillez sélectionner une formation.',
            'first-name'   => 'Prénom trop court.',
            'last-name'    => 'Nom trop court.',
            'email'        => 'Adresse email invalide.',
            'jobs'         => 'Veuillez renseigner votre fonction.',
            'company-name' => 'Veuillez renseigner le nom de la structure.',
            'company-type' => 'Veuillez renseigner le type de structure.',
            'financier'    => 'Veuillez sélectionner une option de financement.',
        ];

        // some of the data is invalid
        if($invalid = invalid($data, $rules, $messages)) {
            $alert = $invalid;

        // the data is fine, let's send the email
        } else {
            try {

                $normalized_firstname = Str::slug($data['first-name']);
                $normalized_lastname = Str::slug($data['last-name']);
                $date = date('Y_m_d');
                $timestamp = date('His');
                $filename = $date . '-' . $timestamp . '-' . $normalized_firstname . '_' . $normalized_lastname . '.md';

                $emailContent = Str::upper('Candidature de ' . esc($data['first-name']) . ' ' . esc($data['last-name']) . ' - ' . date('d/m/Y') . "\n") .

                    "\n----------------------------------\n" .
                    Str::upper("INFORMATIONS PERSONNELLES\n") .
                    "----------------------------------\n" .
                    'Formation souhaitée : ' . esc($data['formation']) . "\n" .
                    'Prénom : ' . esc($data['first-name']) . "\n" .
                    'Nom : ' . esc($data['last-name']) . "\n" .
                    'Email : ' . esc($data['email']) . "\n" .
                    (!empty($data['phone']) ? 'Téléphone : ' . esc($data['phone']) . "\n" : '') .
                    (!empty($data['address']) || !empty($data['postcode']) || !empty($data['city']) || !empty($data['state']) ?
                        'Adresse : ' .
                        (!empty($data['address']) ? esc($data['address']) . ', ' : '') .
                        (!empty($data['postcode']) ? esc($data['postcode']) . ' ' : '') .
                        (!empty($data['city']) ? esc($data['city']) . ' ' : '') .
                        (!empty($data['state']) ? '(' . esc($data['state']) . ')' : '') . "\n" : '') .

                    "\n----------------------------------\n" .
                    Str::upper("INFORMATIONS PROFESSIONNELLES\n") .
                    "----------------------------------\n" .
                    (!empty($data['company-name']) ? 'Nom de la structure : ' . esc($data['company-name']) . "\n" : '') .
                    (!empty($data['company-type']) ? 'Type de structure : ' . esc($data['company-type']) . "\n" : '') .
                    (!empty($data['jobs']) ? 'Fonction : ' . esc($data['jobs']) . "\n" : '') .
                    (!empty($data['company-address']) ? 'Adresse de la structure : ' . esc($data['company-address']) . "\n" : '') .
                    (!empty($data['company-phone']) ? 'Téléphone de la structure : ' . esc($data['company-phone']) . "\n" : '') .
                    (!empty($data['company-siret']) ? 'Numéro SIRET : ' . esc($data['company-siret']) . "\n" : '') .

                    "\n----------------------------------\n" .
                    Str::upper("INFORMATIONS FINANCIÈRES\n") .
                    "----------------------------------\n" .
                    (!empty($data['financier']) ? 'Financement : ' . esc($data['financier']) . "\n" : '') .
                    (!empty($data['comment']) ? "\nCommentaire :\n" . esc($data['comment']) . "\n" : '');

                file_put_contents($page->root() . '/' . $filename, $emailContent);

                $kirby->email([
                    'template' => 'submission--continue',
                    'from'     => 'candidature@esaj.asso.fr',
                    'replyTo'  => $data['email'],
                    'to'       => 'formationcontinue@esaj.asso.fr',
                    'subject'  => 'Nouvelle candidature : ' . esc($data['first-name']) . ' ' . esc($data['last-name']),
                    'data'     => [
                      'firstName'      => esc($data['first-name']),
                      'lastName'       => esc($data['last-name']),
                      'email'          => esc($data['email']),
                      'formation'      => esc($data['formation']),
                      'phone'          => esc($data['phone']),
                      'address'        => esc($data['address']),
                      'postcode'       => esc($data['postcode']),
                      'city'           => esc($data['city']),
                      'state'          => esc($data['state']),
                      'from'           => esc($data['from']),
                      'jobs'           => esc($data['jobs']),
                      'companyName'    => esc($data['company-name']),
                      'companyType'    => esc($data['company-type']),
                      'companyAddress' => esc($data['company-address']),
                      'companyPhone'   => esc($data['company-phone']),
                      'companySiret'   => esc($data['company-siret']),
                      'financier'      => esc($data['financier']),
                      'comment'        => esc($data['comment']),
                    ],
                ]);

            } catch (Exception $error) {
                if(option('debug')):
                    $alert['error'] = 'Le formulaire n\'a pas pu être soumis: <strong>' . $error->getMessage() . '</strong>';
                else:
                    $alert['error'] = 'Le formulaire n\'a pas pu être soumis !';
                endif;
            }

            if (empty($alert) === true) {
                $success = 'Votre candidature a bien été envoyée. Nous vous contacterons dans les meilleurs délais.';
                $data = [];
            }
        }
    }

    return [
        'alert'   => $alert,
        'data'    => $data ?? false,
        'success' => $success ?? false,
    ];
};
