<?php

return function ($kirby, $pages, $page) {

    $alert = null;

    if($kirby->request()->is('POST') && get('submit')) {

        // check the honeypot
        if(empty(get('website')) === false) {
            go($page->url());
            exit;
        }

        $data = [
            'company'   => get('company'),
            'type'      => get('type'),
            'title'     => get('title'),
            'city'      => get('city'),
            'startDate' => get('startDate') ?? '',
            'email'     => get('email') ?? '',
            'link'      => get('link') ?? '',
            'file'      => $kirby->request()->files()->get('file')['size'] > 0 ? $kirby->request()->files()->get('file') : false,
        ];


        $rules = [
            'company' => ['required', 'minLength' => 3],
            'type'    => ['required', 'minLength' => 3],
            'title'   => ['required', 'minLength' => 3],
            'city'    => ['required', 'minLength' => 3],
            'email'   => ['required', 'email'],
        ];

        $messages = [
            'company' => 'Veuillez renseigner le nom de l\'entreprise.',
            'type'    => 'Veuillez choisir un type d\'offre.',
            'title'   => 'Veuillez renseigner l\'intitulé du post.',
            'city'    => 'Veuillez renseigner la localisation du poste (Ville).',
            'email'   => 'Veuillez renseigner un email de contact.',
        ];

        // some of the data is invalid
        if($invalid = invalid($data, $rules, $messages)) {
            $alert = $invalid;

        // the data is fine, let's send the email
        } else {
            // authenticate as almighty
            $kirby->impersonate('kirby');

            try {
                $newJobPage = $page->parent()->createChild(
                    [
                    'slug'     => Str::slug($data['title']),
                    'template' => 'job',
                    'content'  => [
                      'title'       => $data['title'],
                      'type'        => $data['type'],
                      'company'     => $data['company'],
                      'location'    => $data['city'],
                      'offerFormat' => $data['file'] ? 'file' : 'link',
                      'offerFile'   => [],
                      'offerLink'   => $data['file'] ? '' : $data['link'],
                    ],
                  ]
                )->publish()->changeStatus('listed');


                if ($data['file']) {
                    $name = crc32($data['file']['name'] . microtime()) . '_' . $data['file']['name'];
                    $file = $newJobPage->createFile([
                        'source'   => $data['file']['tmp_name'],
                        'filename' => $name,
                        'template' => 'upload',
                        'content'  => [
                            'date' => date('Y-m-d H:i'),
                        ],
                    ]);

                    if ($file) {
                        $newJobPage->update(['offerFile' => [$file->uuid()]]);
                    }
                }
            } catch (Exception $error) {
                if(option('debug')):
                    $alert['error'] = 'Le formulaire n\'a pas pu être soumis: <strong>' . $error->getMessage() . '</strong>. File ' . $error->getFile() . ' line ' . $error->getLine();
                else:
                    $alert['error'] = 'Le formulaire n\'a pas pu être soumis !';
                endif;
            }

            if (empty($alert) === true) {
                $success = 'Votre offre a bien été ajoutée.';
                $data = [];
            }
        }
    }

    return [
        'alert'   => $alert,
        'data'    => $data ?? false,
        'success' => $success ?? false,
    ];
};
