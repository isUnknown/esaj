<?php snippet('header') ?>
<main style="--color: <?=  getColor($page)?>">
  <div class="content-header">
    <?php if ($page->parent()->template() == 'category'): ?>
    <h4 class="category"><?= $page->parent()->title() ?></h4>
    <?php endif ?>
    <h2><?= $page->title() ?></h2>
    <?php if ($page->information()->isNotEmpty()): ?>
    <div class="information">
      <?= $page->information() ?>
    </div>
    <?php endif ?>
    <?php if ($page->year()->isNotEmpty()): ?>
    <p class="year">Scolarité <?= $page->year() ?></p>
    <?php endif ?>
  </div>

  <?php if($success): ?>
  <div class="alert success">
    <p><?= $success ?></p>
  </div>
  <?php else: ?>
  <?php if (isset($alert['error'])): ?>
  <div><?= $alert['error'] ?></div>
  <?php endif ?>
  <form method="post" action="<?= $page->url() ?>">
    <section class="honeypot">
      <label for="website">Website <abbr title="required">*</abbr></label>
      <input type="url" id="website" name="website" tabindex="-1">
    </section>

    <section class="fields">
      <h2>Vous êtes</h2>
      <div class="double">
        <input type="text" id="first-name" placeholder="Prénom*" name="first-name"
          value="<?= esc($data['first-name'] ?? '', 'attr') ?>"
          required>
        <input type="text" id="last-name" placeholder="Nom*" name="last-name"
          value="<?= esc($data['last-name'] ?? '', 'attr') ?>"
          required>
      </div>
      <div class="labelized">
        <label for="from">Comment avez-vous connu notre formation :</label>
        <input type="text" id="from" placeholder="Réseaux sociaux etc." name="from"
          value="<?= esc($data['from'] ?? '', 'attr') ?>">
      </div>
      <div class="labelized">
        <label for="formation-select">Quelle formation vous intéresse :</label>
        <select name="formation" id="formation-select" required>
          <option value="" disabled selected>--Choisissez une option--*</option>
          <?php foreach($page->formations()->toStructure() as $formation): ?>
          <option value="<?= $formation->name() ?>">
            <?= $formation->name() ?></option>
          <?php endforeach ?>
        </select>
      </div>
      <?= isset($alert['first-name']) ? '<span class="alert error">' . esc($alert['first-name']) . '</span>' : '' ?>
      <?= isset($alert['name']) ? '<span class="alert error">' . esc($alert['name']) . '</span>' : '' ?>
    </section>

    <section class="fields">
      <h2>Coordonnées personnelles</h2>
      <p class="privacy">Ces informations ne seront pas divulguées en-dehors de l'école</p>
      <div class="double">
        <input type="email" type="email" id="email" autocomplete="email" placeholder="E-mail*" name="email"
          value="<?= esc($data['email'] ?? '', 'attr') ?>"
          required>
        <input type="phone" id="phone" placeholder="Téléphone" name="phone"
          value="<?= esc($data['phone'] ?? '', 'attr') ?>">
      </div>
      <input type="text" id="address" placeholder="Adresse" name="address"
        value="<?= esc($data['address'] ?? '', 'attr') ?>">

      <div class="double">
        <input type="text" id="postcode" placeholder="Code postal" name="postcode"
          value="<?= esc($data['postcode'] ?? '', 'attr') ?>">
        <input type="text" id="city" placeholder="Ville" name="city"
          value="<?= esc($data['city'] ?? '', 'attr') ?>">
      </div>

      <input type="text" id="state" placeholder="Pays" name="state"
        value="<?= esc($data['state'] ?? '', 'attr') ?>">

      <?= isset($alert['email']) ? '<span class="alert error">' . esc($alert['email']) . '</span>' : '' ?>
    </section>

    <section class="fields">
      <h2>Structure professionnelle</h2>
      <input type="text" id="jobs" placeholder="Fonction*" name="jobs"
        value="<?= esc($data['jobs'] ?? '', 'attr') ?>"
        required>
      <div class="double">
        <input type="text" id="company-name" placeholder="Nom de la structure*" name="company-name"
          value="<?= esc($data['company-name'] ?? '', 'attr') ?>"
          required>
        <input type="text" id="company-type" placeholder="Type de structure*" name="company-type"
          value="<?= esc($data['company-type'] ?? '', 'attr') ?>"
          required>
      </div>
      <input type="text" id="company-address" placeholder="Adresse de la structure" name="company-address"
        value="<?= esc($data['company-address'] ?? '', 'attr') ?>">
      <div class="double">
        <input type="text" id="company-phone" placeholder="Tél. de la structure" name="company-phone"
          value="<?= esc($data['company-phone'] ?? '', 'attr') ?>">
        <input type="text" id="company-siret" placeholder="SIRET" name="company-siret"
          value="<?= esc($data['company-siret'] ?? '', 'attr') ?>">
      </div>
      <?= isset($alert['jobs']) ? '<span class="alert error">' . esc($alert['jobs']) . '</span>' : '' ?>
      <?= isset($alert['company-name']) ? '<span class="alert error">' . esc($alert['company-name']) . '</span>' : '' ?>
      <?= isset($alert['company-type']) ? '<span class="alert error">' . esc($alert['company-type']) . '</span>' : '' ?>
    </section>

    <section class="fields">
      <h2>Financement</h2>
      <div class="labelized">
        <label for="financier-select">Qui financera cette formation :</label>
        <select name="financier" id="financier-select" required>
          <option value="" disabled selected>--Choisissez une option--*</option>
          <?php foreach($page->financiers()->split() as $financier): ?>
          <option value="<?= $financier?>">
            <?= $financier ?></option>
          <?php endforeach ?>
        </select>
      </div>
      <?= isset($alert['financier']) ? '<span class="alert error">' . esc($alert['financier']) . '</span>' : '' ?>
    </section>

    <section class="fields">
      <h2>Commentaire</h2>
      <input type="text" id="comment" placeholder="Quelque chose à ajouter ?" name="comment"
        value="<?= esc($data['comment'] ?? '', 'attr') ?>">
    </section>
    </section>

    <section class="agreement">
      <label for="agreement">
        <input type="checkbox" id="agreement" name="agreement"
          value="<?= esc($data['agreement'] ?? '', 'attr') ?>"
          required>
        Accord pour que nous prenions contact avec vous.
        <?= isset($alert['agreement']) ? '<span class="alert error">' . esc($alert['agreement']) . '</span>' : '' ?>
      </label>
    </section>
    <input type="submit" name="submit" value="Envoyer">
  </form>
  <?php endif ?>
</main>
<?php if ($kirby->request()->is('POST') && get('submit')): ?>
<?php snippet('cookie-scripts', ['internal' => [
      "gtag('event', 'conversion', {'send_to': 'AW-674861644/mC_kCNO6_p8ZEMyk5sEC'});",
    ]]) ?>
<?php endsnippet() ?>
<?php endif ?>
<?php snippet('footer') ?>