<?php snippet('header') ?>
<main <?= e($page->parent()->color()->exists(), 'style="--color: ' . $page->parent()->color() . '"') ?>
  x-data="{
  search: '',
  filters: [],
  get isNoSearch() {
  const isNoSearch = this.search.length === 0 && this.filters.length === 0
  return isNoSearch
  },
  toggleTag(tag) {
  if (this.filters.some(filter => filter === tag)) {
  this.filters = this.filters.filter(filter => filter !== tag)
  } else {
  this.filters.push(tag)
  }
  }
  }"
  >
  <div class="content-header">
    <?php if ($page->parent()->template() == 'category'): ?>
    <h4 class="category"><?= $page->parent()->title() ?></h4>
    <?php endif ?>
    <h2><?= $page->title() ?></h2>
    <div class="intro intro--list">
      <?= $page->intro() ?>
    </div>
  </div>

  <?php snippet('search', [
      'placeholder' => 'offre, entreprise, contrat…',
  ]) ?>

  <section class="list list--jobs">
    <?php foreach($page->children()->sortBy('published', 'desc') as $job): ?>
    <!-- Ne pas supprimer les replace(). Les " ' " délimite les chaînes de caratères. -->
    <li class="list__item" :class="isVisible ? '': 'hidden'" x-data="{
      title: '<?= Str::replace($job->title(), '\'', '') ?>'.toLowerCase(),
      location: '<?= Str::replace($job->location(), '\'', '') ?>'.toLowerCase(),
      company: '<?= Str::replace($job->company(), '\'', '') ?>'.toLowerCase(),
      type: '<?= Str::replace($job->type(), '\'', '') ?>'.toLowerCase(),
      get isVisible() {
        const searchLower = search.toLowerCase().replace('\'', '');
        
        const isTitleMatch = this.title.toLowerCase().includes(searchLower);
        const isLocationMatch = this.location.toLowerCase().includes(searchLower);
        const isCompanyMatch = this.company.toLowerCase().includes(searchLower);
        const isTypeMatch = this.type.toLowerCase().includes(searchLower);

        return isNoSearch || (search.length > 0 && (isTitleMatch || isCompanyMatch || isLocationMatch || isTypeMatch));
      }
    }">
      <?php
        $link = null;
        if ($job->offerFormat() == 'link' && $job->offerLink()->isNotEmpty()) {
            $link = $job->offerLink();
        }
        if ($job->offerFormat() == 'file' && $job->offerFile()->isNotEmpty()) {
            $link = $job->offerFile()->toFile()->url();
        }
        ?>
      <a href="<?= $link ?>" title="Voir l'annonce" target="_blank">
        <p class="job__description"><?= $job->title() ?> ·
          <?= $job->company() ?>
        </p>
        <p class="job__infos"><?= $job->type() ?> ·
          <?= $job->location() ?> ·
          <?= $job->published()->toDate('d/m/Y') ?>
        </p>
      </a>
    </li>
    <?php endforeach ?>
  </section>
</main>

<?php snippet('footer') ?>