<?php snippet('header') ?>
<main <?= e($page->parent()->color()->exists(), 'style="--color: ' . $page->parent()->color() . '"') ?>
  x-data="{
  search: '',
  filters: [],
  get isNoSearch() {
  const isNoSearch = this.search.length === 0 && this.filters.length === 0
  return isNoSearch
  },
  toggleTag(tag) {
  if (this.filters.some(filter => filter === tag)) {
  this.filters = this.filters.filter(filter => filter !== tag)
  } else {
  this.filters.push(tag)
  }
  }
  }"
  >
  <div class="content-header">
    <?php if ($page->parent()->template() == 'category'): ?>
    <h4 class="category"><?= $page->parent()->title() ?></h4>
    <?php endif ?>
    <h2><?= $page->title() ?></h2>
    <div class="intro intro--list">
      <?= $page->intro() ?>
    </div>
  </div>

  <?php snippet('search', [
      'placeholder' => 'nom, statut, localisation…'
  ]) ?>

  <section class="list list--directory">
    <?php foreach($page->items()->toStructure() as $item): ?>

    <!-- Ne pas supprimer les replace(). Les " ' " délimite les chaînes de caratères. -->
    <li class="list__item" :class="isVisible ? '': 'hidden'" x-data="{
      name: '<?= Str::replace($item->name(), '\'', '') ?>'.toLowerCase(),
      location: '<?= Str::replace($item->location(), '\'', '') ?>'.toLowerCase(),
      status: '<?= Str::replace($item->status(), '\'', '') ?>'.toLowerCase(),
      get isVisible() {
        const searchLower = search.toLowerCase().replace('\'', '');
        
        const isNameMatch = this.name.toLowerCase().includes(searchLower);
        const isLocationMatch = this.location.toLowerCase().includes(searchLower);
        const isStatusMatch = this.status.toLowerCase().includes(searchLower);

        return isNoSearch || (search.length > 0 && (isNameMatch || isStatusMatch || isLocationMatch));
      }
    }">
      <span class="name">
        <p><?= $item->name() ?></p>
      </span>
      <span class="tags">
        <p>
          <?= Str::replace($item->status(), ', ', ' / ') ?><?= e($item->location()->isNotEmpty(), ' · ' . $item->location()) ?>
        </p>
      </span>
      <span
        class="link"><?= e($item->link()->isNotEmpty(), '<a href="' . $item->link()->toUrl() .'" title="Aller à ' . $item->link()->toUrl() . '">en savoir +</a>') ?></span>
    </li>
    <?php endforeach ?>
  </section>
</main>

<?php snippet('footer') ?>