<h2><?= $firstName ?> <?= $lastName ?> a candidaté via le site internet.</h2>
<em>Vous pouvez lui écrire en répondant directement à ce mail.</em>

<h3>Informations sur <?= $firstName ?> :</h3>
Formation souhaitée : <?= e($formation === 'initial', 'initiale', 'continue') ?><br />
Prénom : <?= $firstName ?><br />
Nom : <?= $lastName ?><br />
Email : <?= $email ?><br />
<?php if ($phone): ?>
    Téléphone : <?= $phone ?><br />
<?php endif ?>
Niveau d'étude actuel : <?= $actualLevel ?><br />
Niveau d'entrée souhaité : <?= $wantedLevel ?><br />

<?php if ($address || $postcode || $city || $state): ?>
  Adresse : 
    <?= e($address, $address . ', ') ?>
    <?= e($postcode, $postcode . ' ') ?>
    <?= e($city, $city . ' ') ?>
    <?= e($state, $state) ?>
    <br />
<?php endif ?>
