<h2>Nouvelle offre d'emploi déposée sur le site internet.</h2>
<em>Vous pouvez contacter son auteur en répondant directement à ce mail.</em>

<h3>Informations sur l'offre :</h3>
<strong>Organisation :</strong> <?= $company ?><br />
<strong>Type :</strong> <?= $type ?><br />
<strong>Nom :</strong> <?= $title ?><br />
<strong>Email :</strong> <?= $email ?><br />

<a href="https://www.esaj.asso.fr/liens-professionels/esaj-emplois">Aller à la page des offres</a>
