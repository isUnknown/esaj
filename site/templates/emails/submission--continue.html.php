<h2><?= $firstName ?> <?= $lastName ?> a candidaté via le site internet.</h2>
<em>Vous pouvez lui écrire en répondant directement à ce mail.</em>

<h3>Informations sur <?= $firstName ?> :</h3>
<strong>Formation souhaitée :</strong> <?= e($formation === 'initial', 'initiale', 'continue') ?><br />
<strong>Prénom :</strong> <?= $firstName ?><br />
<strong>Nom :</strong> <?= $lastName ?><br />
<strong>Email :</strong> <?= $email ?><br />

<?php if ($phone): ?>
    <strong>Téléphone :</strong> <?= $phone ?><br />
<?php endif ?>

<?php if ($address || $postcode || $city || $state): ?>
    <strong>Adresse :</strong> 
    <?= e($address, $address . ', ') ?>
    <?= e($postcode, $postcode . ' ') ?>
    <?= e($city, $city . ' ') ?>
    <?= e($state, '(' . $state . ')') ?><br />
<?php endif ?>

<h3>Informations professionnelles :</h3>
<?php if ($jobs): ?>
    <strong>Fonction :</strong> <?= $jobs ?><br />
<?php endif ?>
<?php if ($companyName): ?>
    <strong>Nom de la structure :</strong> <?= $companyName ?><br />
<?php endif ?>
<?php if ($companyType): ?>
    <strong>Type de structure :</strong> <?= $companyType ?><br />
<?php endif ?>
<?php if ($companyAddress): ?>
    <strong>Adresse de la structure :</strong> <?= $companyAddress ?><br />
<?php endif ?>
<?php if ($companyPhone): ?>
    <strong>Téléphone de la structure :</strong> <?= $companyPhone ?><br />
<?php endif ?>
<?php if ($companySiret): ?>
    <strong>Numéro SIRET :</strong> <?= $companySiret ?><br />
<?php endif ?>

<h3>Informations financières :</h3>
<?php if ($financier): ?>
    <strong>Financement :</strong> <?= $financier ?><br />
<?php endif ?>

<?php if ($comment): ?>
<h3>Commentaire :</h3>
<p><?= nl2br($comment) ?></p>
<?php endif ?>
