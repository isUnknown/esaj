<?php snippet('header') ?>
<main style="--color: <?=  getColor($page)?>">
  <div class="content-header">
    <?php if ($page->parent()->template() == 'category'): ?>
    <h4 class="category"><?= $page->parent()->title() ?></h4>
    <?php endif ?>
    <h2><?= $page->title() ?></h2>
    <?php if ($page->information()->isNotEmpty()): ?>
    <div class="information">
      <?= $page->information() ?>
    </div>
    <?php endif ?>
    <?php if ($page->year()->isNotEmpty()): ?>
    <p class="year">Scolarité <?= $page->year() ?></p>
    <?php endif ?>
  </div>

  <?php if($success): ?>
  <div class="alert success">
    <p><?= $success ?></p>
  </div>
  <?php else: ?>
  <?php if (isset($alert['error'])): ?>
  <div><?= $alert['error'] ?></div>
  <?php endif ?>
  <form method="post" action="<?= $page->url() ?>">
    <section class="honeypot">
      <label for="website">Website <abbr title="required">*</abbr></label>
      <input type="url" id="website" name="website" tabindex="-1">
    </section>
    <section class="fields">
      <h2>Vous êtes</h2>
      <div class="double">
        <input type="text" id="first-name" placeholder="Prénom*" name="first-name"
          value="<?= esc($data['first-name'] ?? '', 'attr') ?>"
          required>
        <input type="text" id="last-name" placeholder="Nom*" name="last-name"
          value="<?= esc($data['last-name'] ?? '', 'attr') ?>"
          required>
        <?= isset($alert['first-name']) ? '<span class="alert error">' . esc($alert['first-name']) . '</span>' : '' ?>
        <?= isset($alert['last-name']) ? '<span class="alert error">' . esc($alert['last-name']) . '</span>' : '' ?>
      </div>
      <div class="double">
        <input type="text" id="actual-level" placeholder="Niveau d'étude actuel*" name="actual-level"
          value="<?= esc($data['actual-level'] ?? '', 'attr') ?>"
          required>
        <input type="text" id="wanted-level" placeholder="Niveau d'entrée souhaité*" name="wanted-level"
          value="<?= esc($data['wanted-level'] ?? '', 'attr') ?>"
          required>
        <?= isset($alert['actual-level']) ? '<span class="alert error">' . esc($alert['actual-level']) . '</span>' : '' ?>
        <?= isset($alert['wanted-level']) ? '<span class="alert error">' . esc($alert['wanted-level']) . '</span>' : '' ?>
      </div>
    </section>

    <section class="fields">
      <h2>Coordonnées</h2>
      <p class="privacy">Ces informations ne seront pas divulguées en-dehors de l'école</p>
      <input type="email" type="email" id="email" autocomplete="email" placeholder="E-mail*" name="email"
        value="<?= esc($data['email'] ?? '', 'attr') ?>"
        required>
      <input type="text" id="phone" placeholder="Téléphone" name="phone"
        value="<?= esc($data['phone'] ?? '', 'attr') ?>">
      <input type="text" id="address" placeholder="Adresse" name="address"
        value="<?= esc($data['address'] ?? '', 'attr') ?>">

      <div class="double">
        <input type="text" id="postcode" placeholder="Code postal" name="postcode"
          value="<?= esc($data['postcode'] ?? '', 'attr') ?>">
        <input type="text" id="city" placeholder="Ville" name="city"
          value="<?= esc($data['city'] ?? '', 'attr') ?>">
      </div>

      <input type="text" id="state" placeholder="Pays" name="state"
        value="<?= esc($data['state'] ?? '', 'attr') ?>">

      <?= isset($alert['email']) ? '<span class="alert error">' . esc($alert['email']) . '</span>' : '' ?>
    </section>

    <section class="agreement">
      <label for="agreement">
        <input type="checkbox" id="agreement" name="agreement"
          <?= isset($data['agreement']) ? 'checked' : '' ?>
        required>
        Accord pour que nous prenions contact avec vous.
        <?= isset($alert['agreement']) ? '<span class="alert error">' . esc($alert['agreement']) . '</span>' : '' ?>
      </label>
    </section>
    <input type="submit" name="submit" value="Envoyer">
  </form>
  <?php endif ?>
</main>
<?php if ($kirby->request()->is('POST') && get('submit')): ?>
<?php snippet('cookie-scripts', ['internal' => [
      "gtag('event', 'conversion', {'send_to': 'AW-674861644/mC_kCNO6_p8ZEMyk5sEC'});",
    ]]) ?>
<?php endsnippet() ?>
<?php endif ?>
<?php snippet('footer') ?>