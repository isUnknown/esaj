<?php snippet('header') ?>
<main style="--color: <?=  getColor($page)?>">  
  <div class="content-header">
  <?php if($success): ?>
    <div class="alert success">
        <p><?= $success ?></p>
    </div>
    <?php else: ?>
    <?php if (isset($alert['error'])): ?>
        <div><?= $alert['error'] ?></div>
    <?php endif ?>
  <?php endif ?>
    <h2><?= $page->title() ?></h2>
  </div>
  <form method="post" action="<?= $page->url() ?>" enctype="multipart/form-data">
      <section class="honeypot">
          <label for="website">Website <abbr title="required">*</abbr></label>
          <input type="url" id="website" name="website" tabindex="-1">
      </section>
      <section class="fields">
          <h2>Votre offre d'emploi</h2>
          <input type="text" id="company" placeholder="Nom de l'entreprise*" name="company" value="<?= esc($data['company'] ?? '', 'attr') ?>" required>
          <?= isset($alert['company']) ? '<span class="alert error">' . esc($alert['company']) . '</span>' : '' ?>
          
          <select name="type" id="type" required>
            <option value="">Type d'offre*</option>
            <?php foreach($page->parent()->jobTypes()->split() as $jobType): ?>
              <option value="<?= $jobType ?>"><?= $jobType ?></option>
            <?php endforeach ?>
          </select>
          <?= isset($alert['type']) ? '<span class="alert error">' . esc($alert['type']) . '</span>' : '' ?>
          
          <input type="text" id="title" placeholder="Intitulé du poste*" name="title" value="<?= esc($data['title'] ?? '', 'attr') ?>" required>
          <?= isset($alert['title']) ? '<span class="alert error">' . esc($alert['title']) . '</span>' : '' ?>
          
          <div class="double">
            <input type="text" id="city" placeholder="Ville*" name="city" value="<?= esc($data['city'] ?? '', 'attr') ?>" required>
            <input type="text" id="startDate" placeholder="Date de début" name="startDate" value="<?= esc($data['startDate'] ?? '', 'attr') ?>">
          </div>
          <?= isset($alert['city']) ? '<span class="alert error">' . esc($alert['city']) . '</span>' : '' ?>

          <input type="email" name="email" autocomplete="email" id="email" placeholder="E-mail*" value="<?= esc($data['email'] ?? '', 'attr') ?>" required>
          <?= isset($alert['email']) ? '<span class="alert error">' . esc($alert['email']) . '</span>' : '' ?>
      </section>
      <section class="fields">
          <h2>Annonce</h2>
          <label for="file">Choisissez un fichier (max 3Mo):</label>
          <input name="file" type="file" accept="application/pdf">
          
          <p>OU</p>
          
          <input type="text" id="link" placeholder="Lien vers l'offre en ligne" name="link" value="<?= esc($data['link'] ?? '', 'attr') ?>">
      </section>

      <input type="submit" name="submit" value="Envoyer">
  </form>
</main>
<?php if ($kirby->request()->is('POST') && get('submit')): ?>
<?php snippet('cookie-scripts', ['internal' => [
      "gtag('event', 'conversion', {'send_to': 'AW-674861644/mC_kCNO6_p8ZEMyk5sEC'});",
    ]]) ?>
<?php endsnippet() ?>
<?php endif ?>
<?php snippet('footer') ?>