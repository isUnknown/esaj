<?php snippet('header') ?>
<section class="banners">
  <ul>
    <?php foreach($page->banners()->toStructure() as $banner): ?>
        <li class="banner">
          <div class="marquee" style="--duration: <?= float_rand(40, 70) ?>s;--offset:<?= float_rand(-10, 40) ?>rem">
            <?php for ($i = 0; $i < 8; $i++) : ?>
              <div class="marquee__item">
                <?php if ($banner->label()->isNotEmpty()): ?>
                  <h4 class="banner__label"><?= $banner->label() ?></h4>
                <?php endif ?>
                <h2 style="--color: <?= $banner->color() ?>;" class="banner__info">
                  <a href="<?= $banner->link()->toUrl() ?>" title="Aller à <?= $banner->link()->toUrl() ?>" <?= e(!str_contains($banner->link()->toUrl(), $site->url()), ' target="_blank"') ?>><?= $banner->info() ?></a>
                </h2>
              </div>
            <?php endfor ?>
          </div>
        </li>
    <?php endforeach ?>
  </ul>
</section>
<main class="main-column-width">
<?php foreach($page->featured()->toLayouts() as $layout): ?>
    <?php snippet('row', ['layout' => $layout]) ?>
<?php endforeach ?>
</main>
<?php snippet('button-links') ?>
<?php snippet('footer') ?>
</body>
</html>