<?php snippet('header') ?>
  <main <?= e($page->parent()->color()->exists(), 'style="--color: ' . $page->parent()->color() . '"') ?>>
    <div class="content-header">
    <?php if ($page->parent()->template() == 'category'): ?>
      <h4 class="category"><?= $page->parent()->title() ?></h4>
    <?php endif ?>
      <h2><?= $page->title() ?></h2>
      <?php if ($page->intro()->isNotEmpty()): ?>
        <div class="intro">
          <?= $page->intro() ?>
        </div>
      <?php endif ?>
    </div>
    <section class="row row--not-divided">
      <div class="grid grid--2">
        <?php foreach($page->children() as $child): ?>
  
          <?php 
            $cover = $child->cover()->exists() && $child->cover()->isNotEmpty() ? 
              $cover = $child->cover()->toFile()
              : $cover = $child->files()->first();
            
            $link = $child->template() == 'link' ? $child->link()->toUrl() : $child->url();
            $target = $child->template() == 'link' ? '_blank' : '';
  
            $color = $child->color()->isNotEmpty() ? $child->color() : "#ffffff00";
          ?>
          
          <div class="column no-line column--large " style="--span:6">
            <?php snippet('card', [
              'link' => $link, 
              'target' => $target, 
              'color' => $color, 
              'image' => $cover,
              'title' => $child->title(),
              'text' => $child->intro(),
              'crop' => [
                "width" => 1100,
                "height" => 580,
              ]
              ]) ?>
          </div>
        <?php endforeach ?>
      </div>
    </section>
  </main>
  <?php snippet('button-links') ?>
</main>
<?php snippet('footer') ?>