<?php snippet('header') ?>
<main <?= e($page->parent()->color()->exists(), 'style="--color: ' . $page->parent()->color() . '"') ?>>
  <div class="content-header">
  <?php if ($page->parent()->template() == 'category'): ?>
    <h4 class="category"><?= $page->parent()->title() ?></h4>
  <?php endif ?>
    <h2><?= $page->title() ?></h2>
    <div class="intro">
      <?= $page->intro() ?>
    </div>
  </div>

  <div class="team">
    <div class="builder team__builder">
    <?php foreach ($page->team()->toLayouts() as $layout): ?>
      <?php snippet('row', ['layout' => $layout]) ?>
    <?php endforeach ?>
    </div>
    
    <div id="team-panel" class="team__panel">
      <div class="team__panel-wrapper">
        <button 
          class="team__panel__close"
          aria-label="Fermer le panneau membre." 
          aria-controls="team-panel" 
          aria-expanded="false"
        >
          <img 
            src="<?= url('assets') ?>/svg/close.svg" 
            alt="Icône de fermeture du panneau de navigation"
          >
        </button>
        <figure class="image-wrapper hidden">
          <img src="">
        </figure>
        <p class="hidden">
        </p>
        <a href="" class="hidden" target="_blank">En savoir +</a>
      </div>
  </div>
</main>
<?php snippet('button-links') ?>
<script>
  document.addEventListener('DOMContentLoaded', () => {
  const memberButtons = document.querySelectorAll('.member__button')
  const members = document.querySelectorAll('.member');
  const panel = document.querySelector('.team__panel');
  const closeBtn = document.querySelector('.team__panel__close')
  const coverNode = panel.querySelector('figure');
  const presentationNode = panel.querySelector('p');
  const linkNode = panel.querySelector('a');

  function showMember(member) {
    const button = member.querySelector('button')
    
    const coverSrc = member.dataset.coverSrc || '';
    const coverSrcset = member.dataset.coverSrcset || '';
    const coverSizes = member.dataset.coverSizes || '';
    const coverWidth = member.dataset.coverWidth || '';
    const coverHeight = member.dataset.coverHeight || '';
    const coverAlt = member.dataset.coverAlt || '';
    
    const presentation = member.dataset.presentation || '';
    const link = member.dataset.link || '';

    memberButtons.forEach(item => item.classList.remove('active'))
    button.classList.add('active')


    coverNode.classList.toggle('hidden', !coverSrc);
    const img = coverNode.querySelector('img')
    img.setAttribute('src', coverSrc);
    img.setAttribute('srcset', coverSrcset);
    img.setAttribute('sizes', coverSizes);
    img.setAttribute('width', coverWidth);
    img.setAttribute('height', coverHeight);
    img.setAttribute('alt', coverAlt);
    
    presentationNode.classList.toggle('hidden', !presentation);
    presentationNode.textContent = presentation;
    
    linkNode.classList.toggle('hidden', !link);
    linkNode.setAttribute('href', link);

    setTimeout(() => {
      panel.classList.add('open')    
    }, 100);
  }

  function handleMemberClick(event) {
    const member = event.currentTarget.closest('.member');

    showMember(member)
  };

  
  if (window.innerWidth > 700) {
    const firstMember = document.querySelector('.member')
    showMember(firstMember)
  }

  
  members.forEach(member => {
    const btn = member.querySelector('button');
    btn.addEventListener('click', handleMemberClick);
    btn.addEventListener('click', handleMemberClick);
  });
  closeBtn.addEventListener('click', () => {
    memberButtons.forEach(item => item.classList.remove('active'))
    panel.classList.remove('open')
  })
});
</script>
<?php snippet('footer') ?>