<?php snippet('header') ?>
<?php
  $color = null;
  if ($page->color()->isNotEmpty()) {
    $color = $page->color();
  } elseif ($page->parent() && $page->parent()->color()->exists() && $page->parent()->color()->isNotEmpty()) {
    $color = $page->parent()->color();
  } else {
    $color = '#000';
  }
?>
  <main style="--color: <?= e(str_contains($color, "#fff"), "#000", $color) ?>;">
    <div class="content-header">
    <?php if ($page->parent() && $page->parent()->template() == 'category'): ?>
      <h4 class="category"><?= $page->parent()->title() ?></h4>
    <?php endif ?>
      <h2><?= $page->title() ?></h2>
      <?php if ($page->intro()->isNotEmpty()): ?>
        <div class="intro">
          <?= $page->intro() ?>
        </div>
      <?php endif ?>
    </div>
    <div class="builder">
    <?php foreach ($page->builder()->toLayouts() as $layout): ?>
      <?php snippet('row', ['layout' => $layout]) ?>
    <?php endforeach ?>
    </div>
  </main>
  <?php snippet('button-links') ?>
</main>
<?php snippet('footer') ?>