<?php

namespace AdrienPayet\FrontComments;

class Comment
{
    private $page;
    private $data;
    private $repo;

    public function __construct($commentData, $page)
    {
        $this->page = $page;
        $this->data = $commentData;
        $this->repo = option('adrienpayet.front-comments.repo');

        $this->repo['service'] = strtolower($this->repo['service']);
        $this->repo['name'] = strtolower($this->repo['name']);
        $this->repo['name'] = str_replace(' ', '-', $this->repo['name']);
    }


    private function _getCreateIssueRequestUrl()
    {
        $id = urlencode($this->repo['owner'] . '/' . $this->repo['name']);

        $baseUrl = null;
        if ($this->repo['service'] === 'github') {
            $baseUrl = "https://api.github.com/repos/";
        } elseif ($this->repo['service'] === 'framagit') {
            $baseUrl = "https://framagit.org/api/v4/projects/";
        } elseif ($this->repo['service'] === 'gitlab') {
            $baseUrl = "https://gitlab.com/api/v4/projects/";
        }

        $url = null;
        if ($this->repo['service'] === 'github') {
            $url = $baseUrl . $id . "/issues?access_token=" . $this->repo['token'];
        } else {
            $url = $baseUrl . $id . "/issues?private_token=" . $this->repo['token'];
        }

        return $url;
    }

    private function _formatData()
    {
        $description = $this->_createDescription();

        $data = array(
          'title' => $this->data['message'],
          'labels' => option('adrienpayet.front-comments.labels')
        );

        if ($this->repo['service'] === 'github') {
            $data['body'] = $description;
        } else {
            $data['description'] = $description;
        }

        return $data;
    }

    private function _createDescription()
    {
        $description = 'Comment created by **' . $this->data['author'] . '** ';
        $description .= 'the **' . $this->data['date'] . '** at ' . $this->data['time'] . ' ';
        $description .= ' in a window of **' . $this->data['windowWidth'] . 'px width**. ';
        $description .= 'User agent: ' . $this->data['userAgent'] . ' . ';
        $description .= '[**SEE THE COMMENT**](' . site()->url() . '/' . $this->page->uri() . '/#' . $this->data['id'] .')';
        return $description;
    }

    public function createIssue()
    {
        $requestUrl = $this->_getCreateIssueRequestUrl();
        $data = $this->_formatData();

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $requestUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json',
            ],
        ]);

        $response = curl_exec($curl);
        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if ($httpCode != 201) {
            throw new Exception("Failed to create issue: {$httpCode}", 1);
        }

        curl_close($curl);

        $responseData = json_decode($response);

        if (isset($responseData->id)) {
            $this->data['issue-id'] = $responseData->iid;
            $this->data['issue-url'] = $this->repo['service'] === 'github' ? $responseData->html_url : $responseData->web_url;
        } else {
            throw new Exception("Invalid response format", 1);
        }
    }


    public function closeIssue()
    {
        $requestUrl = $this->_getCloseIssueRequestUrl();

        $data = null;
        if ($this->repo['service'] === 'github') {
            $data = json_encode([
                'state' => 'closed'
            ]);
        } else {
            $data = json_encode([
                'state_event' => 'close'
            ]);
        }

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $requestUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $this->repo['service'] === 'github' ? 'PATCH' : 'PUT',
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json',
            ],
        ]);

        $response = curl_exec($curl);
        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if ($httpCode != 200) {
            throw new Exception("Failed to close issue: {$httpCode}", 1);
        }

        curl_close($curl);
    }

    private function _getCloseIssueRequestUrl()
    {
        $id = urlencode($this->repo['owner'] . '/' . $this->repo['name']);

        $baseUrl = null;
        if ($this->repo['service'] === 'github') {
            $baseUrl = "https://api.github.com/repos/";
        } elseif ($this->repo['service'] === 'framagit') {
            $baseUrl = "https://framagit.org/api/v4/projects/";
        } elseif ($this->repo['service'] === 'gitlab') {
            $baseUrl = "https://gitlab.com/api/v4/projects/";
        }

        $issueId = $this->data['issue-id'];

        $url = null;
        if ($this->repo['service'] === 'github') {
            $url = $baseUrl . $id . "/issues/" . $issueId . "?access_token=" . $this->repo['token'];
        } else {
            $url = $baseUrl . $id . "/issues/" . $issueId . "?private_token=" . $this->repo['token'];
        }

        return $url;
    }


    public function id()
    {
        return $this->data['id'];
    }

    public function data()
    {
        return $this->data;
    }

    public function hasIssue()
    {
        return isset($this->data['issue-id']) && strlen($this->data['issue-id']) > 0;
    }

}
