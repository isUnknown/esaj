<?php

use AdrienPayet\FrontComments\Comment;

function rec_copy($source, $destination)
{
    if (is_dir($source)) {
        if (!file_exists($destination)) {
            mkdir($destination, 0755, true);
        }
        $files = scandir($source);
        foreach ($files as $file) {
            if (!str_starts_with($file, '.')) {
                rec_copy("$source/$file", "$destination/$file");
            }
        }
    } else {
        try {
            copy($source, $destination);
        } catch (\Throwable $th) {
            throw new Exception($th->getMessage(), 1);
        }
    }
}

function deleteDir(string $dirPath): void
{
    if (!is_dir($dirPath)) {
        throw new InvalidArgumentException("$dirPath must be a directory");
    }
    if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
        $dirPath .= '/';
    }
    $files = glob($dirPath . '*', GLOB_MARK);
    foreach ($files as $file) {
        if (is_dir($file)) {
            deleteDir($file);
        } else {
            unlink($file);
        }
    }
    rmdir($dirPath);
}

function setFiles($location)
{
    $destination = __DIR__ . '/../../../../assets/front-comments';

    if ($location === 'media') {
        if (is_dir($destination)) {
            deleteDir($destination);
        }
    } else {
        $source = __DIR__ . '/../assets';
        if (!is_dir($destination)) {
            mkdir($destination, 0755, true);
        }

        rec_copy($source, $destination);
    }
}

function getCommentData($comments, $commentId)
{

    $comment = array_filter(
        $comments,
        function ($item) use ($commentId) {
            return $item['id'] == $commentId;
        }
    );

    $comment = current($comment);

    return $comment;

}

function deleteComment($pageUri, $commentId)
{
    $page = Find::page($pageUri);

    $comments = $page->comments()->toData('yaml');

    $newComments = [];
    foreach ($comments as $item) {
        $comment = new Comment($item, $page);
        if ($comment->id() == $commentId) {
            if ($comment->hasIssue()) {
                $comment->closeIssue();
            }
        } else {
            $newComments[] = $comment->data();
        }
    }

    $newPage = $page->update(
        [
          'comments' => $newComments
        ]
    );

    return $newPage->comments()->toData('yaml');
}
