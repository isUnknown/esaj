<?php

return [
  'adrienpayet.front-comments.comments' => 'Commentaires',
  'adrienpayet.front-comments.remove' => 'Supprimer',
  'adrienpayet.front-comments.see' => 'Voir',
  'adrienpayet.front-comments.see-issue' => 'Voir le ticket',
  'adrienpayet.front-comments.create-issue' => 'Créer un ticket',
  'adrienpayet.front-comments.confirm-create-issue' => 'Voulez-vous vraiment créer un ticket ?',
  'adrienpayet.front-comments.confirm-delete-comment' => 'Voulez-vous vraiment supprimer ce commentaire ?',
  'adrienpayet.front-comments.author' => 'Auteur',
  'adrienpayet.front-comments.message' => 'Message',
  'adrienpayet.front-comments.date' => 'Date',
  'adrienpayet.front-comments.time' => 'Heure',
  'adrienpayet.front-comments.no-comment' => 'Aucun commentaire',
  'adrienpayet.front-comments.add-comment' => 'Ajouter un commentaire',
];
