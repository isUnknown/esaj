<?php

return [
  'adrienpayet.front-comments.comments' => 'Comments',
  'adrienpayet.front-comments.remove' => 'Remove',
  'adrienpayet.front-comments.see' => 'See',
  'adrienpayet.front-comments.see-issue' => 'Go to the issue',
  'adrienpayet.front-comments.create-issue' => 'Create an issue',
  'adrienpayet.front-comments.confirm-create-issue' => 'Do you really want to create an issue ?',
  'adrienpayet.front-comments.confirm-delete-comment' => 'Do you really want to remove this comment ?',
  'adrienpayet.front-comments.author' => 'Author',
  'adrienpayet.front-comments.message' => 'Message',
  'adrienpayet.front-comments.date' => 'Date',
  'adrienpayet.front-comments.time' => 'Time',
  'adrienpayet.front-comments.add-comment' => 'Add a comment',
];
