import CommentPanel from "./edition-panels/CommentPanel.js";
import Store from "../store.js";

/**
 * Class representing the Add Button.
 */
class AddButton {
  /**
   * Create an AddButton.
   * @param {string} selector - The CSS selector for the button.
   */
  constructor(position) {
    this._button = this._createButton(position);
    this._initEventListeners();
  }

  /**
   * Create and return a new button element with the specified HTML content.
   * @return {HTMLButtonElement} The created button element.
   * @private
   */
  _createButton(position) {
    const div = document.createElement("div");
    div.innerHTML = `<button class="fc__btn fc__btn-add fc__btn-add--${position} fc__btn-add--free" title="New comment">
      <img class="fc__icon fc__plus" src="${Store.filesPath}/icons/chat-box-plus.svg">
    </button>`;
    const button = div.firstChild;
    document.body.appendChild(button);
    return button;
  }

  /**
   * Initialize the event listeners for the button.
   * @private
   */
  _initEventListeners() {
    this._button.addEventListener("click", this._handleClick.bind(this));
    window.addEventListener("mousemove", this._handleMouseMove.bind(this));
    window.addEventListener("keydown", this._handleKeyDown.bind(this));
  }

  /**
   * Handle the click event on the button.
   * @private
   */
  _handleClick(event) {
    if (this._button.classList.contains("fc__btn-add--move")) {
      const commentPanel = new CommentPanel();
      commentPanel.create();
      this._resetPosition();
    }
    this._button.classList.toggle("fc__btn-add--move");
  }

  /**
   * Handle the mousemove event on the window.
   * @private
   */
  _handleMouseMove(event) {
    if (this._button.classList.contains("fc__btn-add--move")) {
      const iconSize = 48;
      this._button.style.left = event.clientX - iconSize / 2 + "px";
      this._button.style.top =
        event.clientY + window.scrollY - iconSize / 2 + "px";
    }
  }

  /**
   * Handle the keydown event on the window.
   * @private
   */
  _handleKeyDown(event) {
    if (event.key === "Escape") {
      this._button.classList.remove("fc__btn-add--move");
      this._resetPosition();
    }
  }

  /**
   * Reset the position of the button.
   * @private
   */
  _resetPosition() {
    this._button.style.left = "";
    this._button.style.top = "";
  }
}

export default AddButton;
