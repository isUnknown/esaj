import Comment from "./classes/comments/Comment.js";
import Panel from "./classes/edition-panels/Panel.js";
import Store from "./store.js";
import AddBtn from "./classes/AddBtn.js";
import SuggestionPanel from "./classes/edition-panels/SuggestionPanel.js";
import Suggestion from "./classes/Suggestion.js";
import CommentBuilder from "./classes/comments/CommentBuilder.js";

document.addEventListener("DOMContentLoaded", () => {
  Store.author = FCAuthor;
  Store.page.id = FCPageId;
  Store.page.uri = FCPageUri;
  Store.csrf = FCCsrf;
  Store.filesPath = FCFilesPath;

  try {
    const comments = JSON.parse(FCComments);
    comments.forEach((item) => {
      const comment = new CommentBuilder()
        .setPosition(item.position)
        .setAuthor(item.author)
        .setMessage(item.message)
        .setId(item.id)
        .setDate(item.date)
        .setTime(item.time)
        .setWindowWidth(item.windowWidth)
        .setUserAgent(item.userAgent)
        .build();

      Store.comments.push(comment);
    });
  } catch (error) {
    console.error("Plugin Front Comments : can't parse comments : ", error);
    console.log("Comments : ", FCComments);
  }

  const addBtn = new AddBtn(FCPosition);

  // const suggestions = JSON.parse(FCSuggestions);
  // suggestions.forEach((item) => {
  //   const suggestion = new Suggestion(
  //     item.author,
  //     item.context,
  //     item.target,
  //     item.suggestion,
  //     item.fieldName,
  //     item.id,
  //     item.date,
  //     item.time
  //   );
  //   Store.suggestions.push(suggestion);
  // });
  // document.querySelectorAll(".fc__field").forEach((field) => {
  //   field.addEventListener("mouseup", (event) => {
  //     const selection = window.getSelection().toString();
  //     if (
  //       selection.length > 0 &&
  //       !event.target.classList.contains("fc__suggestion-bubble")
  //     ) {
  //       const suggestionPanel = new SuggestionPanel(field);
  //       suggestionPanel.create();
  //     }
  //   });
  // });
});
