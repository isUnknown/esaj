const Store = {
  comments: [],
  suggestions: [],
  author: null,
  page: {
    id: null,
    uri: null,
  },
  csrf: null,
  save(key) {
    const data = {};
    data[key] = this[key];
    const init = {
      method: "PATCH",
      headers: {
        "X-CSRF": this.csrf,
      },
      body: JSON.stringify(data).replaceAll("_", ""),
    };

    return fetch("/api/pages/" + this.page.id, init)
      .then((response) => {
        if (!response.ok) {
          return Promise.reject("Error updating page: " + response.statusText);
        }
        return response.json();
      })
      .then((response) => {
        console.log(response);
        console.log("Page successfully updated.");
      })
      .catch((error) => {
        return Promise.reject(error);
      });
  },
};

export default Store;
