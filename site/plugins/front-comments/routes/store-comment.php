<?php

function storeComment()
{
    $jsonRequest = file_get_contents("php://input");
    $request = json_decode($jsonRequest, true);

    if (isset($request['position']) && is_string($request['position'])) {
        $request['position'] = json_decode($request['position'], true);
    }

    $dataFile = __DIR__ . '/../data/data.json';
    $data = file_get_contents($dataFile);
    $jsonData = json_decode($data, true);

    $foundIndex = null;
    foreach ($jsonData as $index => $item) {
        if ($item['id'] === $request['id']) {
            $foundIndex = $index;
            break;
        }
    }

    if (is_null($foundIndex)) {
        $jsonData[] = $request;
    } else {
        $jsonData[$foundIndex] = $request;
    }

    file_put_contents($dataFile, json_encode($jsonData));
    return $jsonData;
}
