<?php

use AdrienPayet\FrontComments\Comment;

return [
    'pattern' => 'comments/delete/(:any)/(:all)',
    'load' => function (string $commentId, string $pageUri) {
        return [
            'component' => 'k-remove-dialog',
            'props' => [
                'text' => t('adrienpayet.front-comments.confirm-delete-comment')
            ]
        ];
    },
    'submit' => function (string $commentId, string $pageUri) {
        deleteComment($pageUri, $commentId);
        return true;
    }
];
