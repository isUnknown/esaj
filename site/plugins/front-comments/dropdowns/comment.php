<?php

return [
    'pattern' => 'comments/(:any)/(:all)',
    'action'  => function (string $commentId, string $pageUri) {
        $options = [
          [
            'text'   => t('adrienpayet.front-comments.see'),
            'icon'   => 'preview',
            'link' => site()->url() . '/' . $pageUri . '/#' . $commentId,
          ],
          [
            'text'   => t('adrienpayet.front-comments.remove'),
            'icon'   => 'remove',
            'dialog' => 'comments/delete/' . $commentId . '/' . $pageUri
          ]
        ];

        if (option('adrienpayet.front-comments.repo.name')) {
            $page = Find::page($pageUri);
            $comments = $page->comments()->toData('yaml');

            $commentData = getCommentData($comments, $commentId);
            $service = strtolower(option('adrienpayet.front-comments.repo.service'));

            if (isset($commentData['issue-id']) && strlen($commentData['issue-id']) > 0) {
                $options[] = [
                  'text' => t('adrienpayet.front-comments.see-issue'),
                  'icon' => $service === 'github' ? 'github' : 'gitlab',
                  'link' => $commentData['issue-url'],
                ];
            } else {
                $options[] = [
                  'text' => t('adrienpayet.front-comments.create-issue'),
                  'icon' => $service === 'github' ? 'github' : 'gitlab',
                  'dialog' => 'comments/create-issue/' . $commentId . '/' . $pageUri
                ];
            }
        }

        return $options;
    }
];
