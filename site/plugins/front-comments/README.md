![Kirby Front Comments plugin cover](./doc/cover.jpg)

# Kirby front comments plugin

The Kirby Front Comments plugin allows you to add comments anywhere in the front-end of your Kirby website, making it easy for admins, developers, designers and other collaborators to communicate and collaborate directly on the site. It automatically integrate contextual information to each comment, such as user agent, window width, date and time, making it a valuable tool for debugging and troubleshooting.

In addition, the plugin can be connected to your GitHub, GitLab (including Framagit) account, allowing you to create and close issues directly from your website. This makes it easy to track and manage issues, and to keep all your collaboration in one place.

## Setup

### Install

**Composer is the recommended way to install the plugin.** Run the following command in your terminal:

```
composer require adrienpayet/front-comments
```

Alternatively, you can download this repository and add it to the /site/plugins directory of your project.

### Include the snippet

Add the snippet anywhere inside the `<head>` of your header: `<?php snippet('front-comments') ?>`.

### Connect with GitHub or GitLab repository (optional)

To create issues directly from comments on the website, add the repository information to the plugin configuration :

`/site/config/config.php`:

```php
'adrienpayet' => [
  'front-comments' => [
    'repo' => [
      'service' => 'github', // REQUIRED - 'github' | 'gitlab' | 'framagit' (case insensive)
      'token' => 'glpat-xxxxxxxxxxxxxxxxxxxx', // REQUIRED - access token (see below)
      'owner' => 'username', // REQUIRED
      'name' => 'repository name', // REQUIRED - 'Your Plugin Name'  | 'your-plugin-name'
      'labels' => ['bug', 'front-end'], // OPTIONAL - ['label 1', 'label 2'] (Default value is set to ['front-comments']).
    ]
  ]
]
```

_Learn how to create and manage personnal access tokens on [GitHub](https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/managing-your-personal-access-tokens) and [GitLab](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#create-a-personal-access-token)._

## Use

### Add new comments

Logged in users can add comments directly in the front-end by clicking the button located by default in the bottom right corner of the screen. See below how to [change its position](#customize).

### Manage comments though the panel

To view and manage all comments, visit `your-website.com/panel/comments`, also is accessible from the default panel menu. To add the comments global view to your custom panel menu, call 'comments' in your `/site/config/config.php` :

```php
'panel' => [
  'menu' => [
    'custom-menu-item' => [...],
    '-',

    'comments'

    'users',
  ]
]
```

From this section, you can create and delete comments, as well as create or visit linked issues.

Please note that **issues can only be created through this section**. However, deleting a comment, whether from the front-end or the panel, will automatically close any linked issues. This ensures that your comments and issues are always synchronized and up-to-date.

## Customize

### Change the add button position

The default position of the add button is set to bottom right. You can change it by passing a `position` property to the snippet :

```php
<?php snippet('front-comments', [
  'position' => 'bottom-right' // default position
  // alternatives :
  'position' => 'bottom-left'
  'position' => 'top-left'
  'position' => 'top-right'
]) ?>
```

## Troubleshooting

### Fix 403 error (rare)

In rare cases, the plugin files may not load due to 403 errors (check the JavaScript console). Fix it by passing a `'location' => 'assets'` property to the snippet:

```php
<?php snippet('front-comments', ['location' => 'assets']) ?>
```

This will copy the files into `/assets/front-comments`, which should resolve the issue.
