<?php

if ($kirby->user()) {
    $position = isset($position) ? $position : 'bottom-right';
    $location = isset($location) ? $location : 'media';

    $filesPath = $location === 'media' ?
      '/media/plugins/adrienpayet/front-comments'
      : '/assets/front-comments';
    setFiles($location);


    echo snippet('front-comments-style', ['filesPath' => $filesPath]);
    echo snippet(
        'front-comments-script',
        [
          'filesPath' => $filesPath,
          'position' => $position
        ]
    );
}
