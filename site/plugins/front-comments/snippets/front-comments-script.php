<?php
$comments = $page->comments()->exists() && $page->comments()->isNotEmpty()
  ? $page->comments()->toData('yaml')
  : [];

$suggestions = $page->suggestions()->exists() && $page->suggestions()->isNotEmpty()
  ? $page->suggestions()->toData('yaml')
  : [];
?>
<script>
  const FCAuthor = '<?= $kirby->user()->name() ?>';
  const FCPageId = '<?= $page->panel()->id() ?>';
  const FCPageUri = '<?= $page->panel()->id() ?>';
  const FCCsrf = "<?= csrf() ?>";
  const FCComments = '<?= json_encode($comments, JSON_HEX_APOS) ?>';
  const FCSuggestions = '<?= json_encode($suggestions, JSON_HEX_APOS) ?>';
  const FCFilesPath = '<?= $filesPath ?>';
  const FCPosition = '<?= $position ?>';
</script>
<?php

          echo js($filesPath . '/index.js');
