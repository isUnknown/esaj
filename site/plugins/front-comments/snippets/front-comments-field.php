<?php if ($kirby->user()): ?>  
  <div class="fc__field" data-field-name="<?= $content->key() ?>" data-content="<?= $content ?>">
    <?= $content ?>
  </div>
<?php else: ?>
  <?= $content ?>
<?php endif ?>