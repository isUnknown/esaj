<?php

Kirby::plugin('adrienpayet/edit-button', [
    'snippets' => [
        'edit-button' => __DIR__ . '/snippets/edit-button.php'
    ]
]);