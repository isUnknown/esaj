<?php

function getRandomColor()
{
    $site = site();
    $colors = $site->colors()->toStructure()->toArray();
    return $colors[array_rand($colors)]['color'];
}

function getColor($pageToCheck)
{
    $color = $pageToCheck->color()->exists() ? $pageToCheck->color() : getRandomColor();
    while ($pageToCheck->parent() && !$pageToCheck->color()->exists()) {
        $pageToCheck = $pageToCheck->parent();
        if ($pageToCheck->color()->exists()) {
            $color = $pageToCheck->color();
            break;
        }
    }
    return $color;
}

function float_rand($min, $max, $round = 2)
{
    $randomFloat = $min + random_int(0, PHP_INT_MAX - 1) / PHP_INT_MAX * ($max - $min);
    return round($randomFloat, $round);
}
