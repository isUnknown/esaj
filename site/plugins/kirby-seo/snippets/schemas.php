<?php

$siteSchema ??= true;
$pageSchema ??= true;

foreach (array_merge($siteSchema ? $site->schemas() : array(), $pageSchema ? $page->schemas() : array()) as $schema) {
    echo $schema;
}
