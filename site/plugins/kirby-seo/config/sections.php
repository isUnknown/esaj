<?php

use Kirby\Toolkit\A;

return array(
  'seo-preview' => array(
    'mixins' => array('headline'),
    'computed' => array(
      'options' => function () {
          return A::map(option('tobimori.seo.previews'), fn ($item) => array(
            'value' => $item,
            'text' => t($item)
          ));
      }
    )
  ),
  'heading-structure' => array(
    'mixins' => array('headline')
  )
);
