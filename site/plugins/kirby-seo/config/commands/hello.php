<?php

use Kirby\CLI\CLI;

return array(
  'description' => 'Hello world',
  'args' => array(),
  'command' => static function (CLI $cli): void {
      $cli->success('Hello world! This command is a preparation for a future release.');
  }
);
