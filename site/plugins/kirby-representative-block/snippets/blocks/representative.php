
<?php

$isExternal = $block->location() == 'external';

$link = '';
if ($isExternal && $block->link()->isNotEmpty()) {  
  $link = $block->link()->toUrl();
} else {
  if ($block->target()->toPage() && $block->target()->toPage()->template() === 'link') {
    $link = $block->target()->toPage()->link()->toUrl();
  } elseif ($block->target()->toPage()) {
    $link = $block->target()->toPage()->url();
  } else {
    $link = '';
  }
}

$target = str_contains($link, $site->url()) ? '' : '_blank';

$color = "#fff";
if (!$isExternal) {
  if ($block->color()->isNotEmpty()) {
    $color = $block->color();
  } else {
    if ($block->target()->isNotEmpty() && $block->target()->toPage()->color()) {
      $color = $block->target()->toPage()->color();
    }
  }
}

$image = null;
if ($block->altCover()->isNotEmpty()) {
  $image = $block->altCover()->toFile();
} else {
  if ($page->target()->isNotEmpty()) {
    if ($block->target()->toPage()->cover()) {
      $image = $block->target()->toPage()->cover()->toFile();
    } else {
      $image = $block->target()->toPage()->files()->first();
    }
  } 
}

snippet('card', array(
  'color' => $color,
  'image'  => $image,
  'title' => $block->altTitle()->isNotEmpty() ? $block->altTitle() : $block->target()->toPage()->title(),
  'text' => $block->altText()->isNotEmpty() ? $block->altText() : $block->target()->toPage()->intro(),
  'link' => $link,
  'target' => $target,
  "crop" => isset($crop) ? $crop : null
));
