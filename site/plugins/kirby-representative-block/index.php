<?php

Kirby::plugin('adrienpayet/kirby-representative-block', array(
  'snippets' => array(
    'blocks/representative' => __DIR__ . '/snippets/blocks/representative.php'
  ),
  'blueprints' => array(
    'blocks/representative' => __DIR__ . '/blueprints/blocks/representative.yml'
  ),
  'blocks' => array(
    'representative' => array()
  ),
));
