(function() {
  "use strict";
  const RepresentativeBlockItem_vue_vue_type_style_index_0_scoped_9d6fc6bb_lang = "";
  function normalizeComponent(scriptExports, render, staticRenderFns, functionalTemplate, injectStyles, scopeId, moduleIdentifier, shadowMode) {
    var options = typeof scriptExports === "function" ? scriptExports.options : scriptExports;
    if (render) {
      options.render = render;
      options.staticRenderFns = staticRenderFns;
      options._compiled = true;
    }
    if (functionalTemplate) {
      options.functional = true;
    }
    if (scopeId) {
      options._scopeId = "data-v-" + scopeId;
    }
    var hook;
    if (moduleIdentifier) {
      hook = function(context) {
        context = context || // cached call
        this.$vnode && this.$vnode.ssrContext || // stateful
        this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext;
        if (!context && typeof __VUE_SSR_CONTEXT__ !== "undefined") {
          context = __VUE_SSR_CONTEXT__;
        }
        if (injectStyles) {
          injectStyles.call(this, context);
        }
        if (context && context._registeredComponents) {
          context._registeredComponents.add(moduleIdentifier);
        }
      };
      options._ssrRegister = hook;
    } else if (injectStyles) {
      hook = shadowMode ? function() {
        injectStyles.call(
          this,
          (options.functional ? this.parent : this).$root.$options.shadowRoot
        );
      } : injectStyles;
    }
    if (hook) {
      if (options.functional) {
        options._injectStyles = hook;
        var originalRender = options.render;
        options.render = function renderWithStyleInjection(h, context) {
          hook.call(context);
          return originalRender(h, context);
        };
      } else {
        var existing = options.beforeCreate;
        options.beforeCreate = existing ? [].concat(existing, hook) : [hook];
      }
    }
    return {
      exports: scriptExports,
      options
    };
  }
  const _sfc_main$1 = {
    __name: "RepresentativeBlockItem",
    props: {
      columns: Number,
      page: Object,
      isChildrenMode: Boolean
    },
    setup(__props) {
      const { page, columns } = __props;
      const link = Vue.computed(() => {
        try {
          return "/pages/" + page.id.replaceAll("/", "+") || null;
        } catch (error) {
          return null;
        }
      });
      const cover = Vue.ref(null);
      const intro = setIntro();
      setCover();
      async function setCover() {
        if (page.content.hasOwnProperty("cover") && page.content.cover.length > 0) {
          cover.value = page.content.cover[0].url;
        } else {
          if (!link.value)
            return;
          cover.value = await fetchFirstFile(link.value);
        }
      }
      function setIntro() {
        if (page.content.hasOwnProperty("intro")) {
          return getFirstParagraph(page.content.intro);
        } else if (page.content.hasOwnProperty("description")) {
          return getFirstParagraph(page.content.description);
        } else {
          return null;
        }
      }
      function getFirstParagraph(text) {
        const div = document.createElement("div");
        div.innerHTML = text;
        const firstParagraph = div.querySelector("p");
        if (firstParagraph) {
          return firstParagraph.textContent;
        } else {
          return div.textContent;
        }
      }
      async function fetchFirstFile(link2) {
        try {
          const { data } = await window.panel.api.get(`${link2}/files`);
          if (data.length > 0) {
            return data[0].url;
          } else {
            return null;
          }
        } catch (error) {
          console.error("Fetch cover image error:", error);
        }
      }
      return { __sfc: true, link, cover, intro, setCover, setIntro, getFirstParagraph, fetchFirstFile };
    }
  };
  var _sfc_render$1 = function render() {
    var _vm = this, _c = _vm._self._c, _setup = _vm._self._setupProxy;
    return !_vm.isChildrenMode ? _c("div", { key: _vm.page.content.title, staticClass: "k-column k-layout-column", style: "--width: 1/" + _vm.columns, attrs: { "id": _vm.page.id, "tabindex": "0" } }, [_c("div", { staticClass: "k-item k-draggable-item", attrs: { "data-has-image": _setup.cover ? "true" : "false", "data-layout": "cards", "data-id": _vm.page.id } }, [_c("figure", { staticClass: "k-frame k-icon-frame k-item-image" }, [_setup.cover ? _c("img", { attrs: { "src": _setup.cover, "alt": "" } }) : _vm._e()]), !_vm.isChildrenMode ? _c("button", { staticClass: "k-button k-sort-handle k-sort-button k-item-sort-handle", attrs: { "data-has-icon": "true", "title": "Déplacer pour réordonner…", "type": "button", "tabindex": "-1" } }, [_c("span", { staticClass: "k-button-icon" }, [_c("svg", { staticClass: "k-icon", attrs: { "aria-hidden": "true", "data-type": "sort" } }, [_c("use", { attrs: { "xlink:href": "#icon-sort" } })])])]) : _vm._e(), _c("div", { staticClass: "k-item-content" }, [_c("h3", { staticClass: "k-item-title" }, [_vm._v(_vm._s(_vm.page.content.title))]), _c("p", { staticClass: "k-info" }, [_vm._v(_vm._s(_setup.intro))])])])]) : _c("div", { key: _vm.page.content.title, staticClass: "k-column k-layout-column", style: "--width: 1/" + _vm.columns, attrs: { "id": _vm.page.id, "tabindex": "0" } }, [_c("div", { staticClass: "blocks" }, [_c("div", { staticClass: "k-block-container k-block-container-fieldset-page k-block-container-type-page" }, [_c("div", { staticClass: "k-block k-block-type-page" }, [_c("div", { staticClass: "k-grid k-layout" }, [_c("div", { staticClass: "k-item k-draggable-item", attrs: { "data-has-image": _setup.cover ? "true" : "false", "data-layout": "cards", "data-id": _vm.page.id, "data-status": _vm.page.status, "data-template": _vm.page.template } }, [_c("figure", { staticClass: "k-frame k-icon-frame k-item-image" }, [_setup.cover ? _c("img", { attrs: { "src": _setup.cover, "alt": "" } }) : _vm._e()]), !_vm.isChildrenMode ? _c("button", { staticClass: "k-button k-sort-handle k-sort-button k-item-sort-handle", attrs: { "data-has-icon": "true", "title": "Déplacer pour réordonner…", "type": "button", "tabindex": "-1" } }, [_c("span", { staticClass: "k-button-icon" }, [_c("svg", { staticClass: "k-icon", attrs: { "aria-hidden": "true", "data-type": "sort" } }, [_c("use", { attrs: { "xlink:href": "#icon-sort" } })])])]) : _vm._e(), _c("div", { staticClass: "k-item-content" }, [_c("h3", { staticClass: "k-item-title" }, [_vm._v(_vm._s(_vm.page.content.title))]), _c("p", { staticClass: "k-info" }, [_vm._v(_vm._s(_setup.intro))])])])])])])])]);
  };
  var _sfc_staticRenderFns$1 = [];
  _sfc_render$1._withStripped = true;
  var __component__$1 = /* @__PURE__ */ normalizeComponent(
    _sfc_main$1,
    _sfc_render$1,
    _sfc_staticRenderFns$1,
    false,
    null,
    "9d6fc6bb",
    null,
    null
  );
  __component__$1.options.__file = "/Users/adrienpayet/code/esaj/site/plugins/kirby-representative-block/src/components/RepresentativeBlockItem.vue";
  const RepresentativeBlockItem = __component__$1.exports;
  const RepresentativeBlock_vue_vue_type_style_index_0_scoped_97d65fa8_lang = "";
  const _sfc_main = {
    __name: "RepresentativeBlock",
    setup(__props) {
      const { content } = __props;
      const pages = Vue.ref([]);
      setContent();
      Vue.watch(content, () => {
        setContent();
      });
      function setContent() {
        if (content.location === "external") {
          content.ischildrenmode = false;
          pages.value = [
            {
              content: {
                title: content.alttitle,
                intro: content.alttext,
                cover: content.altcover
              },
              url: content.link,
              id: Date.now()
            }
          ];
        } else {
          setPages();
        }
      }
      async function setPages() {
        const target = content.target[0];
        if (!target)
          return;
        if (!content.ischildrenmode) {
          try {
            const page = await fetchPage(target.link);
            setTimeout(() => {
              pages.value = [
                {
                  content: {
                    title: content.alttitle.length > 0 ? content.alttitle : page.content.title,
                    intro: content.alttext.length > 0 ? content.alttext : page.content.intro ? page.content.intro : page.content.description ? page.content.description : "",
                    cover: content.altcover.length > 0 ? content.altcover : page.content.cover
                  },
                  url: page.url,
                  id: page.id
                }
              ];
            }, 100);
          } catch (error) {
            console.error("Couldn't fetch target:", error);
          }
        } else {
          try {
            const request = await window.panel.api.get(
              `${target.link}/children?select=content,id,template`
            );
            const children = request.data;
            pages.value = children;
          } catch (error) {
            console.error("Couldn't fetch target children:", error);
          }
        }
      }
      async function fetchPage(link) {
        try {
          const page = await window.panel.api.get(link);
          return page;
        } catch (error) {
          console.error("Fetch target error:", error);
        }
      }
      return { __sfc: true, pages, setContent, setPages, fetchPage, RepresentativeBlockItem };
    }
  };
  var _sfc_render = function render() {
    var _vm = this, _c = _vm._self._c, _setup = _vm._self._setupProxy;
    return _c("section", { attrs: { "tabindex": "0" } }, [_setup.pages.length > 0 ? _vm._l(_setup.pages, function(page) {
      return _c(_setup.RepresentativeBlockItem, { key: page.id, attrs: { "page": page, "columns": _setup.pages.length, "isChildrenMode": _vm.content.ischildrenmode } });
    }) : _vm._e()], 2);
  };
  var _sfc_staticRenderFns = [];
  _sfc_render._withStripped = true;
  var __component__ = /* @__PURE__ */ normalizeComponent(
    _sfc_main,
    _sfc_render,
    _sfc_staticRenderFns,
    false,
    null,
    "97d65fa8",
    null,
    null
  );
  __component__.options.__file = "/Users/adrienpayet/code/esaj/site/plugins/kirby-representative-block/src/components/RepresentativeBlock.vue";
  const RepresentativeBlock = __component__.exports;
  window.panel.plugin("adrienpayet/kirby-representative-block", {
    blocks: {
      representative: RepresentativeBlock
    }
  });
})();
