import RepresentativeBlock from "./components/RepresentativeBlock.vue";

window.panel.plugin("adrienpayet/kirby-representative-block", {
  blocks: {
    representative: RepresentativeBlock,
  },
});
