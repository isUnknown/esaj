# Kirby page block plugin

This plugin add a page block.

## Cover image

Cover field if it exists, otherwise the first file of the target page.

## Text

Intro field if it exists, otherwise description field.

## Style

Use `.k-block-type-page` to style the card.

## Development

1. Install a fresh Kirby StarterKit
2. `cd site/plugins`
3. `git clone` this repo

## Warranty

This plugin is work in progress and comes without any warranty. Use at your own risk.
