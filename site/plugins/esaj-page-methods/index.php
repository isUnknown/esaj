<?php
require_once 'classes/Section.php';

Kirby::plugin('adrienpayet/esaj-page-methods', array(
    'pageMethods' => array(
        'sections' => function () {
          $sections = [];
          if ($this->builder()->exists() && $this->builder()->isNotEmpty()) {
            foreach ($this->builder()->toLayouts() as $row) {
              if ($row->attrs()->isInNav() == 'true') {
                $section = new Section($row->attrs()->sectionTitle());
                $sections[] = $section;
              }
            }
          }
          return $sections;
        },
        'hasSections' => function() {
          return count($this->sections()) > 0;
        },
        'templateTitle' => function () {
            return $this->template()->title();
        }
    )
));
