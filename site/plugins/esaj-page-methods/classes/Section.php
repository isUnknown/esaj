<?php
class Section {
    private $title;
    private $id;

    public function __construct($title) {
        $this->title = $title;
        $this->id = Str::slug($title);
    }

    public function title() {
        return $this->title;
    }

    public function id() {
        return $this->id;
    }
}