import DataSection from "./components/DataSection.vue";

window.panel.plugin("adrienpayet/esaj-page-data", {
  sections: {
    data: DataSection,
  },
});
