<?php

Kirby::plugin('adrienpayet/esaj-page-data', array(
  'sections' => array(
    'data' => array(
      'computed' => array(
        'color' => function () {
            if ($this->model()->parent() && $this->model()->parent()->color()->exists()) {
                return $this->model()->parent()->color()->value();
            } else {
                return '#000';
            }
        }
      )
    )
  ),
));
