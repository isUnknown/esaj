<?php

return [
  'debug' => true,
  'panel' => [
    'css' => 'assets/css/custom-panel.css',
    'js'  => 'assets/js/custom-panel.js',
  ],

  'email' => [
    'transport' => [
        'type'     => 'smtp',
        'host'     => 'ssl0.ovh.net',
        'port'     => 465,
        'security' => 'ssl',
        'auth'     => true,
        'username' => 'candidature@esaj.asso.fr',
        'password' => 'FUpbca76Q397TK0KA',
    ],
  ],

  'thumbs' => [
    'presets' => [
      'default' => [
        'width'   => 600,
        'quality' => 80,
        'format'  => 'webp',
      ],
    ],
    'srcsets' => [
      'default' => [
        '600w'  => ['width' => 600, 'quality' => 80, 'format' => 'webp'],
        '800w'  => ['width' => 800, 'quality' => 80, 'format' => 'webp'],
        '1024w' => ['width' => 1024, 'quality' => 80, 'format' => 'webp'],
        '1440w' => ['width' => 1440, 'quality' => 80, 'format' => 'webp'],
        '2048w' => ['width' => 2048, 'quality' => 80, 'format' => 'webp'],
      ],
    ],
  ],

  'routes' => [
    [
      'method'  => 'GET|POST',
      'pattern' => '/liens-professionels/esaj-emplois/deposer',
      'action'  => function () {
          $jobs = page('page://Z6nXzaaNEok0GwK2');

          return Page::factory(
              [
              'slug'     => 'deposer',
              'template' => 'new-job',
              'parent'   => $jobs,
              'content'  => [
                  'title' => 'Déposer une offre',
                  'uuid'  => Kirby\Uuid\Uuid::generate(),
              ]]
          );
      },
    ],
    [
      'pattern' => 'sitemap.xml',
      'action'  => function () {
          $pages = site()->pages()->index();

          // fetch the pages to ignore from the config settings,
          // if nothing is set, we ignore the error page
          $ignore = kirby()->option('sitemap.ignore', ['error']);

          $content = snippet('sitemap', compact('pages', 'ignore'), true);

          // return response with correct header type
          return new Kirby\Cms\Response($content, 'application/xml');
      },
    ],
    [
      'pattern' => 'sitemap',
      'action'  => function () {
          return go('sitemap.xml', 301);
      },
    ],
  ],
  'sitemap.ignore' => ['error'],

  //================================================================ PLUGINS
  'tobimori.seo.canonicalBase' => 'https://www.esaj.asso.fr/',
  'tobimori.seo.lang'          => 'fr_FR',
  'adrienpayet'                => [
    'front-comments' => [
      'repo' => [
        'service' => 'framagit',
        'token'   => 'glpat-kPdBKFZUyBY_3go61UBz',
        'owner'   => 'isUnknown',
        'name'    => 'esaj',
      ],
    ],
  ],
];
