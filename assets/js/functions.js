const enableBurger = () => {
  const burger = document.querySelector(".burger");
  const panelNav = document.querySelector(".nav-panel");
  const closeBtn = document.querySelector(".nav-panel__close");

  // =========================================== OPEN NAV PANEL
  burger.addEventListener("click", (event) => {
    panelNav.classList.add("open");
    burger.classList.add("open");
    burger.setAttribute("aria-expanded", "true");
    event.stopPropagation();
  });

  // =========================================== CLOSE NAV PANEL
  function closeNavPanel() {
    panelNav.classList.remove("open");
    burger.classList.remove("open");
    burger.setAttribute("aria-expanded", "false");
  }

  window.addEventListener("keyup", (e) => {
    if (e.key === "Escape") {
      closeNavPanel();
    }
  });

  window.addEventListener("click", () => {
    closeNavPanel();
  });

  closeBtn.addEventListener("click", (event) => {
    closeNavPanel();
    event.stopPropagation();
  });
};

const debounce = (func, wait) => {
  let timeout;
  return (...args) => {
    clearTimeout(timeout);
    timeout = setTimeout(() => {
      func.apply(this, args);
    }, wait);
  };
};

function toggleSmallHeaderState() {
  const mainHeader = document.querySelector(".main-header");
  mainHeader.classList.toggle("main-header--small", window.scrollY >= 210);
}

const openSubmenuBg = debounce(() => {
  const mainHeader = document.querySelector(".main-header");
  mainHeader.classList.add("open");
}, 100);

const closeSubmenuBg = debounce(() => {
  const mainHeader = document.querySelector(".main-header");
  mainHeader.classList.remove("open");
}, 100);

function toggleShowMore(event) {
  const fullTextWrapper = event.target
    .closest(".card")
    .querySelector(".card-text__full");
  fullTextWrapper.classList.toggle("visible");
}

export {
  enableBurger,
  debounce,
  toggleSmallHeaderState,
  openSubmenuBg,
  closeSubmenuBg,
  toggleShowMore,
};
