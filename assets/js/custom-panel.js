function classifyTryptichs() {
  const rows = document.querySelectorAll(".k-layout-columns");

  if (!rows) return;

  rows.forEach((row) => {
    const thirds = row.querySelectorAll(
      '.k-layout-column[style="--width: 1/3;"]'
    );

    if (thirds.length === 3) {
      row.classList.add("triptych");
    }
  });
}

function addDataVariables() {
  if (!document.querySelector(".page-data")) return;
  const data = document.querySelector(".page-data").dataset;
  const grid = document.querySelector(".k-grid");

  grid.style = "--color: " + data.color;
}

document.addEventListener("DOMContentLoaded", () => {
  setTimeout(() => {
    classifyTryptichs();
    addDataVariables();
  }, 150);
});
