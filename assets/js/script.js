import {
  enableBurger,
  debounce,
  toggleSmallHeaderState,
  openSubmenuBg,
  closeSubmenuBg,
  toggleShowMore,
} from "./functions.js";

document.addEventListener("DOMContentLoaded", () => {
  // =========================================== HEADER
  const smallLogo = document.querySelector(".logo-small");

  enableBurger();

  const isHome = window.location.origin + "/" === window.location.href;
  if (isHome) {
    window.addEventListener("scroll", debounce(toggleSmallHeaderState, 10));
  }

  document
    .querySelector(".main-nav")
    .addEventListener("mouseover", openSubmenuBg);
  document
    .querySelector(".main-nav")
    .addEventListener("mouseout", closeSubmenuBg);

  const navBtns = document.querySelectorAll(".main-nav ul li");

  navBtns.forEach((navBtn) => {
    navBtn.addEventListener("mouseenter", () => {
      navBtns.forEach((btn) => {
        if (btn !== navBtn) {
          btn.classList.add("bw");
        }
      });
    });

    navBtn.addEventListener("mouseleave", () => {
      navBtns.forEach((btn) => {
        btn.classList.remove("bw");
      });
    });
  });

  document.querySelectorAll(".read-more").forEach((btn) => {
    btn.addEventListener("click", (event) => {
      const truncatedBlock =
        event.target.parentNode.parentNode.querySelector(".block--truncated");
      truncatedBlock.classList.add("open");
    });
  });

  const showMoreBtns = document.querySelectorAll(".card__toggle-full-text");
  showMoreBtns.forEach((btn) => {
    btn.addEventListener("click", toggleShowMore);
  });

  // Cookie banner
  if (!sessionStorage.getItem("cookieAccepted")) {
    document.querySelector(".simple-cookie-banner").classList.remove("hidden");
  }
  document
    .querySelector(".simple-cookie-banner__accept")
    .addEventListener("click", () => {
      sessionStorage.setItem("cookieAccepted", true);
      window.location.reload();
    });
  document
    .querySelector(".simple-cookie-banner__deny")
    .addEventListener("click", () => {
      sessionStorage.removeItem("cookieAccepted");
      window.location.reload();
    });
});
