<?php

// define which files should be analyzed
$finder = PhpCsFixer\Finder::create()
    ->exclude(__DIR__ . '/site/plugins')
    ->in(__DIR__ . '/site')
    ->in(__DIR__ . '/site/plugins/kirby-page-block')
    ->in(__DIR__ . '/site/plugins/esaj-page-methods')
;

$config = new PhpCsFixer\Config();
return $config
    ->setRules(array(
        '@PSR12' => true,
        'array_syntax' => array('syntax' => 'long'),
        'method_chaining_indentation' => true,
    ))
    ->setRiskyAllowed(true)
    ->setFinder($finder)
;
